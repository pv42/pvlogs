use std::cmp::{max, min};
use std::collections::HashMap;

use crate::components::boon_table::{BoonTable, BoonTableRowData};
use chrono::{DateTime, Datelike, IsoWeek, NaiveDate, Utc};
use elite_insight_json::gw2_data::{self, GW2SkillID};
use gloo_console::log;
use yew::prelude::*;
use yew::virtual_dom::VNode;
use yew_bootstrap::component::{Button, ButtonSize, Spinner};
use yew_bootstrap::util::Color as BTSColor;
use crate::yew_main::WING_COLORS;

use crate::components::custom_header_accordion::{
    CustomHeaderAccordion, CustomHeaderAccordionItem,
};

use crate::components::pie_chart::{HtmlColor, PieChart};
use crate::indexed_db_binding::IdbHandle;
use crate::short_log_data::{ShortLogData, ShortLogInfo};

pub enum TimelineMessage {
    Loaded(LogsLoadedMsg),
    Loading,
}

pub struct LogsLoadedMsg {
    logs: HashMap<NaiveDate, Vec<ShortLogInfo>>,
    last_loaded_week: IsoWeek,
}

#[derive(Properties, PartialEq)]
pub struct TimelineProps {}

pub struct Timeline {
    is_loading: bool,
    logs: Vec<(NaiveDate, Vec<ShortLogInfo>)>,
    oldest_week_loaded: IsoWeek,
}

impl Component for Timeline {
    type Message = TimelineMessage;
    type Properties = TimelineProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Timeline {
            is_loading: true,
            logs: vec![],
            oldest_week_loaded: Utc::now().iso_week(),
        }
    }
    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            TimelineMessage::Loaded(msg) => {
                for (date, logs) in msg.logs {
                    self.logs.push((date, logs))
                }
                self.logs.sort_by(|(date1, _), (date2, _)| date2.cmp(date1));
                self.oldest_week_loaded = msg.last_loaded_week;
                self.is_loading = false;

                true
            }
            TimelineMessage::Loading => {
                self.is_loading = true;
                true
            }
        }
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        if self.is_loading && self.logs.is_empty() {
            html! {
                <div class="d-flex align-items-center justify-content-center mt-3">
                    <strong>{"Loading ..."}</strong>
                    <Spinner style={BTSColor::Primary} class="ms-3"/>
                </div>
            }
        } else {
            let scale = 8800.0;
            let days = self.logs.iter().map(|(date, logs)| {
                let mut logs_sorted = logs.clone();
                logs_sorted.sort_by(|log1, log2| log1.data.start_time.cmp(&log2.data.start_time));
                let (logs_html, boon_table_data, wing_data, start, end) = process_logs(&logs_sorted, scale);
                
                let avg_uptime = boon_table_data.iter().map(|btr| (btr.alac1 + btr.alac2 + btr.quickness1 + btr.quickness2 + btr.might1 * 4. + btr.might2 * 4.) / 6.).sum::<f64>() / boon_table_data.len() as f64;
                let mut wing_data: Vec<_>= wing_data.into_iter().collect();
                wing_data.sort_by(|(_, (sa, _ ,_ )),(_, (sb, _ ,_ ))| sa.cmp(sb));
                
                let duration =  end - start;
                let dur_hrs = duration.num_hours();
                let dur_mins = duration.num_minutes() % 60;
                let dur_secs = duration.num_seconds() % 60;
                let date_ident = date.format("%d%m%y");
                let wing_tabs = wing_data.iter().map(|(wing, _)|{
                    html!{
                    <li class="nav-item">
                        <button class="nav-link" id={format!("wing-header-{wing}")} data-bs-toggle="tab" data-bs-target={format!("#wing{wing}-{date_ident}")} type="button" role="tab" aria-controls={format!("wing{wing}-{date_ident}")} aria-selected="true">
                        {format!("W{wing}")}</button>
                    </li>
                    }
                });
                let wing_tables = wing_data.iter().map(|(wing, (_start, _end, logs))| {
                        let logs_rows = logs.iter().map(|log| {
                            let boss_name = gw2_data::get_encounter_by_id(&log.data.boss_id).map(|e| e.name).unwrap_or_else(||log.data.boss_id.clone());
                            let mins = log.data.get_duration().num_minutes();
                            let secs = ((log.data.get_duration().num_milliseconds() % 60000) as f64) / 1000.0;
                            html! {
                                <tr>
                                    <td>{boss_name}</td>
                                    <td>{ format!("{mins}:{secs:05.2}")}</td>
                                    <td><a href={log.url.clone()}>{"Log"}</a></td>
                                </tr>
                            }
                        });
                        html! {
                            <div class="tab-pane fade" id={format!("wing{wing}-{date_ident}")} role="tabpanel" aria-labelledby={format!("wing-header-{wing}-{date_ident}")}>
                                <table>
                                { for logs_rows}
                                </table>
                            </div>
                        }
                    }
                );
                let chart_data: Vec<_> = wing_data.iter().map(|(wing, (start, end, _))| {
                    let color = HtmlColor(WING_COLORS[*wing as usize].to_owned());
                    let data = (*end - start).num_seconds() as f64;
                    let label = format!("Wing{wing}");
                    (color, data, label)
                }).collect();
                html_nested! {
                    <CustomHeaderAccordionItem header={html!{
                        <>
                            <span style="line-height:16px;display:inline-block;margin-right:5px">
                                { date.format("%d.").to_string()}
                                <br/>
                                { date.format("%m.").to_string()}
                            </span>
                            { for logs_html}
                        </>
                    }}>
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-2">
                                <table>
                                    <tr>
                                        <td>{ "Start" }</td>
                                        <td>{ start.format("%H:%M:%S %Z").to_string() }</td>
                                    </tr><tr>
                                        <td>{ "End" } </td>
                                        <td>{ end.format("%H:%M:%S %Z").to_string() } </td>
                                    </tr><tr>
                                        <td>{ "Duration" } </td>
                                        <td>{ format!("{dur_hrs}:{dur_mins:02}:{dur_secs:02}") } </td>
                                    </tr><tr>
                                        <td>{ "Kills" } </td>
                                        <td>{ logs.len() } </td>
                                    </tr><tr>
                                        <td>{ "Avg. Boons" } </td>
                                        <td>{ format!("{avg_uptime:.1}%")}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-auto">
                                <PieChart data={chart_data}/>
                            </div>
                            <div class="col col-lg-5">
                                <ul class="nav nav-tabs" role="tablist">
                                    { for wing_tabs}
                                </ul>
                                <div class="tab-content" id="wing-tables">
                                    { for wing_tables }
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <BoonTable data={boon_table_data} />
                        </div>
                    </CustomHeaderAccordionItem>
                }
            });
            //let current_logs = self.logs;
            let current_last_loaded_week = self.oldest_week_loaded;
            let link = ctx.link().clone();
            let load_older_logs = {
                Callback::from(move |_: _| {
                    link.send_message(TimelineMessage::Loading);
                    link.send_future(async move { Self::load_logs(current_last_loaded_week).await })
                })
            };
            html! {
                <div class="overflow-auto">
                    <div class="container mt-2" >
                        <CustomHeaderAccordion id={format!("extra-data")}>
                        { for days }
                        </CustomHeaderAccordion>
                        <div class="d-flex align-items-center justify-content-center mt-3">
                        if self.is_loading {
                            <Spinner style={BTSColor::Primary} class="ms-3"/>
                        } else {
                            <Button text="Load older logs" onclick={load_older_logs} size={ButtonSize::Large}/>
                        }
                        </div>
                    </div>
                </div>
            }
        }
    }
    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            let oldest_week = self.oldest_week_loaded;
            ctx.link()
                .send_future(async move { Self::load_logs(oldest_week).await });
            self.is_loading = true;
        }
    }
}

fn process_logs<'a>(
    logs_sorted: &'a Vec<ShortLogInfo>,
    scale: f64,
) -> (
    Vec<VNode>,
    Vec<BoonTableRowData>,
    HashMap<
        i32,
        (
            DateTime<chrono::prelude::Utc>,
            DateTime<chrono::prelude::Utc>,
            Vec<&'a ShortLogInfo>,
        ),
    >,
    DateTime<Utc>,
    DateTime<Utc>,
) {
    let mut logs_html = vec![];
    let mut last_end = 0;
    let mut wing_data: HashMap<
        i32,
        (
            DateTime<chrono::prelude::Utc>,
            DateTime<chrono::prelude::Utc>,
            Vec<&ShortLogInfo>,
        ),
    > = HashMap::new();
    let mut boon_table_data = vec![];
    for (index, log) in logs_sorted.iter().enumerate() {
        let encounter = gw2_data::get_encounter_by_id(&log.data.boss_id);
        let wing = encounter.clone().map(|enc| enc.wing).unwrap_or(0);
        let name = encounter
            .map(|enc| enc.short_name)
            .unwrap_or_else(|| log.data.boss_id.clone());
        let mut spacer_width = (log.data.start_time - last_end) as f64 / scale;
        if spacer_width > 400.0 {
            spacer_width = 100.0;
        }
        logs_html.push(html!{
                        <>
                        if index > 0 {
                            <span class="timeline-spacer" style={format!("width: {spacer_width:.1}px;")}></span>
                        }
                        <span style={format!("width: {:.1}px; display: inline-block; line-height: 16px; overflow: hidden; background:linear-gradient(var(--successful-bg-color) 80%, {} 20%); height: 40px;", log.data.duration as f64 / scale, WING_COLORS[wing as usize])}>{name.clone()}</span>
                        </>
                    });
        last_end = log.data.start_time + log.data.duration;

        wing_data
            .entry(wing)
            .and_modify(|(start, end, logs)| {
                *start = min(*start, log.data.get_start_time());
                *end = max(*end, log.data.get_end_time());
                logs.push(log);
            })
            .or_insert((
                log.data.get_start_time(),
                log.data.get_end_time(),
                vec![log],
            ));
        let group_offset = if log.data.players.iter().filter(|x| x.sub_group == 1).count() == 0 {
            2
        } else {
            1
        };
        let quickness1 = log
            .data
            .get_group_uptime(GW2SkillID::QuicknessBuff as i32, group_offset);
        let quickness2 = log
            .data
            .get_group_uptime(GW2SkillID::QuicknessBuff as i32, group_offset + 1);
        let alac1 = log
            .data
            .get_group_uptime(GW2SkillID::AlacrityBuff as i32, group_offset);
        let alac2 = log
            .data
            .get_group_uptime(GW2SkillID::AlacrityBuff as i32, group_offset + 1);
        let might1 = log
            .data
            .get_group_uptime(GW2SkillID::MightBuff as i32, group_offset);
        let might2 = log
            .data
            .get_group_uptime(GW2SkillID::MightBuff as i32, group_offset + 1);
        if log.data.boss_id != "bk"
            && log.data.boss_id != "tc"
            && log.data.boss_id != "esc"
            && log.data.boss_id != "trio"
            && log.data.boss_id != "rr"
        {
            boon_table_data.push(BoonTableRowData {
                boss_name: name,
                quickness1,
                quickness2,
                alac1,
                alac2,
                might1,
                might2,
            });
        }
    }
    let start = logs_sorted
        .first()
        .map(|log| log.data.get_start_time())
        .unwrap_or_default();
    let end = logs_sorted
        .last()
        .map(|log| log.data.get_end_time())
        .unwrap_or_default();
    (logs_html, boon_table_data, wing_data, start, end)
}

impl Timeline {
    async fn load_logs(oldest_week: IsoWeek) -> TimelineMessage {
        let indexed_db = IdbHandle::open().await;
        log!("Loading timeline logs ...");
        let mut logs = HashMap::new();
        let num_weeks = 10;
        let mut week = oldest_week;
        for _ in 0..num_weeks {
            let logs_week = indexed_db
                .get_logs_filtered_grouped_by_week(
                    week,
                    |log: &ShortLogData| {
                        gw2_data::get_encounter_by_id(&log.boss_id)
                            .map(|log| log.wing)
                            .unwrap_or(11)
                            <= 10
                    },
                    |log_id: String, data| ShortLogInfo {
                        url: format!("https://dps.report/{}", log_id),
                        data,
                    },
                )
                .await;
            if let Some(logs_week) = logs_week {
                for log in logs_week {
                    logs.entry(log.data.date())
                        .and_modify(|v: &mut Vec<ShortLogInfo>| v.push(log.clone()))
                        .or_insert(vec![log]);
                }
            }
            let mon = NaiveDate::from_isoywd_opt(week.year(), week.week(), chrono::Weekday::Mon)
                .expect("Invalid week date");
            week = mon.pred_opt().expect("Invalid Date").iso_week()
        }
        TimelineMessage::Loaded(LogsLoadedMsg {
            logs,
            last_loaded_week: week,
        })
    }
}
