use chrono::{DateTime, Utc};
use cmp::Ordering;
use gloo_utils::window;
use std::cmp;
use yew::prelude::*;
use yew_bootstrap::component::Spinner;
use yew_bootstrap::util::Color as BTSColor;

use crate::builds::BuildGuesser;
use crate::builds::PlayerBuildData;
use crate::components::boss_details::gen_player_build_icons;
use crate::components::boss_details::get_color_from_gradient;
use crate::components::table_v2::TableColAlignment;
use crate::components::table_v2::TableHeaderEntry;
use crate::components::table_v2::TableV2;
use crate::indexed_db_binding;
use crate::indexed_db_binding::IdbHandle;
use crate::short_log_data::ShortLogInfo;
use crate::views::boss_view::BossesNav2;
use elite_insight_json::ei_dto::ei_data_types::EIWeaponType;
use elite_insight_json::gw2_data;
use elite_insight_json::gw2_data::DamageModID;
use elite_insight_json::gw2_data::GW2Encounter;
use elite_insight_json::gw2_data::GW2SkillID;

#[derive(Properties, PartialEq)]
pub struct BuildViewProps {
    pub build_hash: u64,
}

pub enum BuildViewMessage {
    BuildLoadedMessage(PlayerBuildData),
    LogsLoadedMsg(Vec<ShortLogInfo>),
    BossSelectedMsg(GW2Encounter),
}

pub struct BuildView {
    loaded_logs: Option<Vec<ShortLogInfo>>,
    selected_boss: Option<GW2Encounter>,
    build: Option<PlayerBuildData>,
}

impl Component for BuildView {
    type Message = BuildViewMessage;
    type Properties = BuildViewProps;

    fn create(_ctx: &Context<Self>) -> Self {
        BuildView {
            loaded_logs: None,
            selected_boss: None,
            build: None,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            BuildViewMessage::LogsLoadedMsg(logs) => {
                self.loaded_logs = Some(logs);
                true
            }
            BuildViewMessage::BossSelectedMsg(encounter) => {
                self.loaded_logs = None;
                self.selected_boss = Some(encounter);
                true
            }
            BuildViewMessage::BuildLoadedMessage(build) => {
                window().document().unwrap().set_title(build.name.as_str());
                self.build = Some(build);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        //let build = BuildGuesser::new()
        //    .get_by_hash(ctx.props().build_hash)
        //    .unwrap();
        let build_hash = ctx.props().build_hash;
        ctx.link().send_future(async move {
            let db = indexed_db_binding::IdbHandle::open().await;
            let build: PlayerBuildData = db.get_build(build_hash).await.expect("Build not in db");
            BuildViewMessage::BuildLoadedMessage(build)
        });
        if let Some(build) = self.build.as_ref() {
            let weapons = build
                .weapons
                .iter()
                .filter(|weapon| !matches!(weapon, EIWeaponType::TwoHand | EIWeaponType::Unknown))
                .map(|weapon| {
                    html! { <> {weapon} {" "} </> }
                });
            let link = ctx.link();
            let on_boss_select = {
                let link = link.clone();
                Callback::from(move |boss: GW2Encounter| {
                    link.send_message(BuildViewMessage::BossSelectedMsg(boss))
                })
            };
            let prof_icon_url_full = format!(
                "/img/highres/120px-{}_icon_(highres).png",
                &build.profession
            );
            let prof_icon_url_small = format!("/img/{}_tango_icon_20px.png", &build.profession);

            let selected_boss = self.selected_boss.clone();
            if selected_boss.is_some() {
                ctx.link().send_future(async move {
                    let selected_boss = selected_boss.clone();
                    let indexed_db = IdbHandle::open().await;
                    let logs = indexed_db
                        .get_logs_for_boss(&selected_boss.unwrap().name_id, |log_id, data| {
                            ShortLogInfo {
                                url: format!("https://dps.report/{}", log_id),
                                data,
                            }
                        })
                        .await
                        .unwrap_or(Vec::new())
                        .into_iter()
                        .filter(|log| {
                            for player in &log.data.players {
                                if player.build_hash == build_hash {
                                    return true;
                                }
                            }
                            false
                        })
                        .collect();
                    BuildViewMessage::LogsLoadedMsg(logs)
                });
            }

            html! {
            <div class="overflow-auto">
            <div class="container mt-2">
                <h1>
                    <img src={prof_icon_url_full} alt={build.profession.clone()} class="icon-build-title"/> { build.name.clone() }
                </h1>
                <table>
                    <tr><td>{"Profession:"}</td><td> <img src={prof_icon_url_small} alt={build.profession.clone()} class="small-icon"/>   {build.profession.clone()}</td></tr>
                    <tr><td>{"Boons:"}</td><td>
                        if build.alac { <img class={"small-icon"} src={"/img/30328_alacrity.png"}/> }
                        if build.quick { <img class={"small-icon"} src={"/img/1187_quickness.png"}/> }
                        if !build.quick && !build.alac {{"-"}}
                    </td></tr>
                    <tr><td>{"Healing:"}</td><td> if build.heal { {"Yes"} } else { {"-"} }</td></tr>
                    <tr><td>{"Damage Type:"}</td><td> { build.damage_type.to_string() } </td></tr>
                    <tr><td>{"Weapons:"}</td><td> { for weapons.clone()} if weapons.count() == 0 { {"Any"} } </td></tr>
                    if build.sc_url.is_some() {
                        <tr><td>{"Snowscrows:"}</td>
                        <td><a href={build.sc_url.clone().unwrap()} target="_blank" alt="Link">

                            {"Go to build guilde"}
                            <span class="crow-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path color="rgb(1 100 200)" d="M456 0c-48.6 0-88 39.4-88 88v29.2L12.5 390.6c-14 10.8-16.6 30.9-5.9 44.9s30.9 16.6 44.9 5.9L126.1 384H259.2l46.6 113.1c5 12.3 19.1 18.1 31.3 13.1s18.1-19.1 13.1-31.3L311.1 384H352c1.1 0 2.1 0 3.2 0l46.6 113.2c5 12.3 19.1 18.1 31.3 13.1s18.1-19.1 13.1-31.3l-42-102C484.9 354.1 544 280 544 192V128v-8l80.5-20.1c8.6-2.1 13.8-10.8 11.6-19.4C629 52 603.4 32 574 32H523.9C507.7 12.5 483.3 0 456 0zm0 64a24 24 0 1 1 0 48 24 24 0 1 1 0-48z"/></svg>
                            </span>
                        </a></td></tr>
                    }
                    if build.benchmark.is_some() {
                        <tr><td>{"Benchmark:"}</td>
                        <td>{build.benchmark}</td></tr>
                    }
                </table>
                <br/>
                <BossesNav2 bosses={gw2_data::get_encounters()} on_click={on_boss_select.clone()} current_boss={ self.selected_boss.clone()}/>
                <div>
                if self.loaded_logs.is_some() {
                    <TableV2<ShortLogInfo> data={self.loaded_logs.clone().unwrap()} table_id="succesful-attempts-tbl" columns={vec![
                            TableHeaderEntry::sortable_w(
                                "Date".to_string(),
                                Box::new(|row: &ShortLogInfo| {
                                    let date: DateTime<Utc> = DateTime::from_timestamp_millis(row.data.start_time).expect("Failed to convert timestamp");
                                    html! {date.format("%d.%m.%Y")}
                                }),
                                Box::new(|a, b| a.data.start_time.cmp(&b.data.start_time)),
                                100
                            ),
                            TableHeaderEntry::sortable_w(
                                "Duration".to_string(),
                                Box::new(|row| {
                                    let mins = row.data.duration / (60 * 1000);
                                    let secs = (row.data.duration as f64 / (1000.0)) % 60.0;
                                    html!{ format!("{:02}:{:0>5.2}", mins, secs)}
                                }),
                                Box::new(|a, b| a.data.duration.cmp(&b.data.duration)),
                                80,
                            ),
                            TableHeaderEntry::sortable(
                                "Alac".to_string(),
                                Box::new(|row| {
                                    gen_player_build_icons(row, &BuildGuesser::new() ,|player|
                                        player.group_buff_generation.get(&(GW2SkillID::AlacrityBuff as i32)).unwrap() +
                                        player.off_group_buff_generation.get(&(GW2SkillID::AlacrityBuff as i32)).unwrap()
                                            >= 30.
                                    )
                                }),
                                // TODO
                                Box::new(|_, _| Ordering::Equal)
                            ),
                            TableHeaderEntry {
                                name: "A%".to_string(),
                                content: Box::new(|row| {
                                    let mut sub_uptime_data = row.data.get_sub_uptimes(GW2SkillID::AlacrityBuff as i32);
                                    sub_uptime_data.sort_by(|(sub1, _ ), (sub2, _ )| sub1.cmp(sub2));
                                    let mut subs_html = vec![];
                                    let mut row_index = 1;
                                    for (sub, uptime) in sub_uptime_data {
                                        let style = format!("background: {}; grid-column: 2; grid-row: {row_index}", get_color_from_gradient(uptime, 50.0, 77.0, 98.0));
                                        subs_html.push(
                                        html!{
                                            <span style={style}> {format!("G{sub}: {uptime:.0}%")} </span>
                                        });
                                        row_index += 1;
                                    }
                                    let uptime = row.data.get_squad_uptime(GW2SkillID::AlacrityBuff as i32);
                                    html!{
                                        <>
                                            <span class="boon-grid-main">
                                            {format!("{:.1}%", uptime)}
                                            </span>
                                            { for subs_html }
                                        </>
                                    }
                                }),
                                cmp_fn:  Box::new(|a, b|
                                    a.data.get_squad_uptime(GW2SkillID::AlacrityBuff as i32).total_cmp(
                                    &b.data.get_squad_uptime(GW2SkillID::AlacrityBuff as i32))),
                                is_sortable: true,
                                bg_color: Some(Box::new(|row|{
                                    let uptime = row.data.get_squad_uptime(GW2SkillID::AlacrityBuff as i32);
                                    get_color_from_gradient(uptime, 50.0, 77.0, 98.0)
                                })),
                                class: Some("boon-grid-td".to_owned()),
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Quick".to_string(),
                                content: Box::new(|row| {
                                    gen_player_build_icons(row, &BuildGuesser::new() ,|player|
                                        player.group_buff_generation.get(&(GW2SkillID::QuicknessBuff as i32)).unwrap() +
                                        player.off_group_buff_generation.get(&(GW2SkillID::QuicknessBuff as i32)).unwrap()
                                            >= 30.
                                    )
                                }),
                                alignment: TableColAlignment::AlignEnd,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Q%".to_string(),
                                content: Box::new(|row| {
                                    html!{ format!("{:.1}%", row.data.get_squad_uptime(GW2SkillID::QuicknessBuff as i32)) }
                                }),
                                cmp_fn:  Box::new(|a, b|
                                    a.data.get_squad_uptime(GW2SkillID::QuicknessBuff as i32).total_cmp(
                                    &b.data.get_squad_uptime(GW2SkillID::QuicknessBuff as i32))),
                                is_sortable: true,
                                bg_color: Some(Box::new(|row|{
                                    let uptime = row.data.get_squad_uptime(GW2SkillID::QuicknessBuff as i32);
                                    get_color_from_gradient(uptime, 50.0, 77.0, 98.0)
                                })),
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Other".to_string(),
                                content: Box::new(|row| {
                                    gen_player_build_icons(row, &BuildGuesser::new() ,|player|
                                        player.group_buff_generation.get(&(GW2SkillID::QuicknessBuff as i32)).unwrap() +
                                        player.off_group_buff_generation.get(&(GW2SkillID::QuicknessBuff as i32)).unwrap() < 30. &&
                                        player.group_buff_generation.get(&(GW2SkillID::AlacrityBuff as i32)).unwrap() +
                                        player.off_group_buff_generation.get(&(GW2SkillID::AlacrityBuff as i32)).unwrap() < 30.
                                    )
                                }),
                                is_sortable: false,
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Might".to_string(),
                                content: Box::new(|row| {
                                    html!{ format!("{:.1}", row.data.get_squad_uptime(GW2SkillID::MightBuff as i32)) }
                                }),
                                cmp_fn:  Box::new(|a, b|
                                    a.data.get_squad_uptime(GW2SkillID::MightBuff as i32).total_cmp(
                                    &b.data.get_squad_uptime(GW2SkillID::MightBuff as i32))),
                                is_sortable: true,
                                bg_color: Some(Box::new(|row|{
                                    let uptime = row.data.get_squad_uptime(GW2SkillID::MightBuff as i32);
                                    get_color_from_gradient(uptime, 13.0, 19.0, 24.5)
                                })),
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Prot".to_string(),
                                content: Box::new(|row| {
                                    html!{ format!("{:.1}%", row.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32)) }
                                }),
                                cmp_fn:  Box::new(|a, b|
                                    a.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32).total_cmp(
                                    &b.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32))),
                                is_sortable: true,
                                bg_color: Some(Box::new(|row|{
                                    let uptime = row.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32);
                                    get_color_from_gradient(uptime, 0.0, 55.0, 98.0)
                                })),
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Sc.".to_string(),
                                content: Box::new(|row| {
                                    html!{ format!("{:.1}%", row.data.get_squad_uptime(DamageModID::Scholar as i32)) }
                                }),
                                cmp_fn: Box::new(|a, b|
                                    a.data.get_squad_uptime(DamageModID::Scholar as i32).total_cmp(
                                    &b.data.get_squad_uptime(DamageModID::Scholar as i32))),
                                is_sortable: true,
                                bg_color: Some(Box::new(|row|{
                                    let uptime = row.data.get_squad_uptime(DamageModID::Scholar as i32);
                                    get_color_from_gradient(uptime, 50.0, 77.0, 98.0)
                                })),
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "#Healer".to_string(),
                                content: Box::new(|row| {
                                    let heal_count = row.data.players.iter().fold(0, |state, player| {
                                        let build = BuildGuesser::new().get_by_hash(player.build_hash);
                                        match build {
                                            None => state,
                                            Some(build) => if build.heal { 1 + state } else { state },
                                        }
                                    });
                                    html!{heal_count}
                                }),
                                is_sortable: false,
                                bg_color: None,
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            },
                            TableHeaderEntry {
                                name: "Log".to_string(),
                                content: Box::new(|row| { html!{ <a href={row.url.clone()} target={"_blank"}>{"Link"}</a> }}),
                                is_sortable: false,
                                bg_color: None,
                                alignment: TableColAlignment::AlignStart,
                                ..Default::default()
                            }
                    ]} />
                } else if self.selected_boss.is_some() {
                    <strong>{"Loading ..."}</strong>
                    <Spinner style={BTSColor::Primary} class="ms-3"/>
                }
                </div>
            </div>
            </div>
            }
        } else {
            html! {
                <div class="d-flex align-items-center justify-content-center mt-3">
                    <strong>{"Loading details ..."}</strong>
                    <Spinner style={BTSColor::Primary} class="ms-3"/>
                </div>
            }
        }
    }
}
