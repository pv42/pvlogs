use std::cmp::Ordering;
use std::ops::Deref;

use gloo_console::error;
use gloo_utils::window;
use yew::prelude::*;
use yew_bootstrap::component::Spinner;
use yew_bootstrap::util::Color as BTSColor;
use yew_router::prelude::Link;

use crate::builds::{BuildGuesser, PlayerBuildData};
use crate::components::table_v2::TableV2;
use crate::components::table_v2::{TableColAlignment, TableHeaderEntry};
use crate::indexed_db_binding;
use crate::yew_main::Route;

#[function_component(BuildsView)]
pub fn builds_view() -> Html {
    // set title
    window().document().unwrap().set_title("Builds");
    let builds: UseStateHandle<Option<Vec<PlayerBuildData>>> = use_state(|| None);
    let b2 = builds.clone();
    wasm_bindgen_futures::spawn_local(async move {
        let db = indexed_db_binding::IdbHandle::open().await;
        match db.get_all_builds().await {
            Ok(builds) => b2.set(Some(builds)),
            Err(err) => {
                error!("Failed to load builds from db", err);
                b2.set(Some(BuildGuesser::new().get_builds()))
            }
        }
    });
    // todo use db builds
    let inner_content = match builds.deref() {
        None => html! {<div class="d-flex align-items-center justify-content-center mt-3">
            <strong>{"Loading details ..."}</strong>
            <Spinner style={BTSColor::Primary} class="ms-3"/>
        </div>},
        Some(builds) => html! {
            <TableV2<PlayerBuildData> table_id="builds" data={builds.clone()} columns={vec![
                TableHeaderEntry::<PlayerBuildData> {
                    name: "Name".to_string(),
                    content: Box::new(|row| html! { <Link<Route> to={Route::Build {build_hash: row.hash}}> {row.name.clone()} </Link<Route>> }),
                    cmp_fn: Box::new(|a, b| a.name.cmp(&b.name)),
                    is_sortable: true,
                    alignment: TableColAlignment::AlignStart,
                    ..Default::default()
                },
                TableHeaderEntry {
                    name: "Profession".to_string(),
                    content: Box::new(|row| html! {
                        <>
                            <img src={format!("/img/{}_tango_icon_20px.png", &row.profession)} alt={row.profession.clone()} class="small-icon"/>
                            {row.profession.clone()}
                        </>}),
                    cmp_fn: Box::new(|a, b| a.profession.cmp(&b.profession)),
                    is_sortable: true,
                    alignment: TableColAlignment::AlignStart,
                    ..Default::default()
                },
                TableHeaderEntry {
                    name: "Boons".to_string(),
                    content: Box::new(|row| html! {
                        <>
                            if row.alac { <img class={"small-icon"} src={"/img/30328_alacrity.png"}/> }
                            if row.quick { <img class={"small-icon"} src={"/img/1187_quickness.png"}/> }
                        </>}),
                    cmp_fn: Box::new(|a, b| a.alac.cmp(&b.alac).then(a.quick.cmp(&b.quick))),
                    is_sortable: true,
                    alignment: TableColAlignment::AlignStart,
                    ..Default::default()
                },
                TableHeaderEntry {
                    name: "Healer".to_string(),
                    content: Box::new(|row| html! {
                        <>
                            if row.heal {{"Yes"}} else {{"-"}}
                        </>}),
                    cmp_fn:  Box::new(|a, b| a.heal.cmp(&b.heal)),
                    is_sortable: true,
                    alignment: TableColAlignment::AlignStart,
                    ..Default::default()
                },
                TableHeaderEntry {
                    name: "Damage Type".to_string(),
                    content: Box::new(|row| html! {row.damage_type.to_string()}),
                    cmp_fn:  Box::new(|a, b| a.damage_type.partial_cmp(&b.damage_type).unwrap_or(Ordering::Equal)),
                    is_sortable: true,
                    alignment: TableColAlignment::AlignStart,
                    ..Default::default()
                },
                TableHeaderEntry {
                    name: "Benchmark".to_string(),
                    content: Box::new(|row| html! {row.benchmark.map(|b| b.to_string()).unwrap_or_default()}),
                    cmp_fn:  Box::new(|a, b| a.benchmark.partial_cmp(&b.benchmark).unwrap_or(Ordering::Equal)),
                    is_sortable: true,
                    alignment: TableColAlignment::AlignStart,
                    ..Default::default()
                }
            ]
            } />
        },
    };
    html! {
        <div class="overflow-auto">
        <div class="container mt-2">
        { inner_content }
        </div>
        </div>
    }
}
