use std::vec;

use chrono::NaiveDate;
use wasm_bindgen::JsCast;
use yew::prelude::*;
use yew_bootstrap::component::Button;
use yew_bootstrap::component::ButtonGroup;
use yew_bootstrap::component::ButtonSize;
use yew_router::prelude::*;

use crate::components::boss_details::BossDetails;
use crate::components::boss_details::{LogCountData, LogDateFilter, LogModeFilter};
use crate::components::build_filter::BuildFilter;
use crate::yew_main::Route;
use crate::yew_main::WING_COLORS;
use elite_insight_json::gw2_data;
use elite_insight_json::gw2_data::GW2Encounter;

#[derive(Properties, PartialEq)]
pub struct BossViewProps {
    pub boss_id: Option<String>,
}

#[function_component(BossView)]
pub fn boss_view(BossViewProps { boss_id }: &BossViewProps) -> Html {
    /*let cholo = PlayerBuildData::new(
        "Condi Holo",
        "Holosmith",
        DamageType::Condi,
        Some("https://snowcrows.com/builds/raids/engineer/condition-holosmith".to_string()),
        None,
    );
    let psb = PlayerBuildData::new(
        "Power Soulbeast",
        "Soulbeast",
        DamageType::Power,
        Some("https://snowcrows.com/builds/raids/ranger/power-soulbeast".to_string()),
        Some(42662),
    );*/
    let filter_builds = use_state(|| vec![/*psb*/]);

    let selected_boss = use_state(|| match boss_id {
        None => None,
        Some(boss_id) => gw2_data::get_encounters()
            .iter()
            .find(|enc| &enc.name_id == boss_id)
            .cloned(),
    });

    let date_filters = use_state(|| LogDateFilter {
        date_after: NaiveDate::from_ymd_opt(2000, 1, 1).unwrap(),
        date_before: NaiveDate::from_ymd_opt(2100, 12, 31).unwrap(),
    });
    let mode_filters = use_state(|| LogModeFilter {
        allow_cm: true,
        allow_nm: true,
        allow_emboldened: true,
    });
    let log_counts = use_state(|| LogCountData {
        logs_loaded: 0,
        logs_total: 0,
        logs_processed: 0,
    });

    let on_boss_select = {
        let selected_boss = selected_boss.clone();
        Callback::from(move |boss: GW2Encounter| selected_boss.set(Some(boss)))
    };

    let details = selected_boss.as_ref().map(|boss| html! {
        <BossDetails boss={boss.clone()} log_counts={log_counts.clone()} date_filter={date_filters.clone()} mode_filter={mode_filters.clone()} build_filter={filter_builds.clone()}/>
    });

    let date_before_change = {
        let date_filters = date_filters.clone();
        Callback::from(move |ev: Event| {
            let target: Option<web_sys::EventTarget> = ev.target();
            let input = target.and_then(|t| t.dyn_into::<web_sys::HtmlInputElement>().ok());
            if let Some(input) = input {
                let date_str: String = input.value();
                let naive = NaiveDate::parse_from_str(&date_str, "%Y-%m-%d").unwrap();
                date_filters.set(LogDateFilter {
                    date_after: date_filters.date_after,
                    date_before: naive,
                });
            }
        })
    };
    let date_after_change = {
        let date_filters = date_filters.clone();
        Callback::from(move |ev: Event| {
            let target: Option<web_sys::EventTarget> = ev.target();
            let input = target.and_then(|t| t.dyn_into::<web_sys::HtmlInputElement>().ok());
            if let Some(input) = input {
                let date_str: String = input.value();
                let naive = NaiveDate::parse_from_str(&date_str, "%Y-%m-%d").unwrap();
                date_filters.set(LogDateFilter {
                    date_after: naive,
                    date_before: date_filters.date_before,
                });
            }
        })
    };
    let set_date_2021 = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2021, 1, 1).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2021, 12, 31).unwrap(),
            });
        })
    };
    let set_date_2022 = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2022, 1, 1).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2022, 12, 31).unwrap(),
            });
        })
    };
    let set_date_2023 = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2023, 1, 1).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2023, 12, 31).unwrap(),
            });
        })
    };
    let set_date_2024 = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2024, 1, 1).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2024, 12, 31).unwrap(),
            });
        })
    };
    let set_date_pof = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2017, 9, 22).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2022, 2, 27).unwrap(),
            });
        })
    };
    let set_date_eod = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2022, 2, 28).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2023, 8, 21).unwrap(),
            });
        })
    };
    let set_date_soto = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2023, 8, 22).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2024, 8, 19).unwrap(),
            });
        })
    };
    let set_date_jw = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2024, 8, 20).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2100, 12, 31).unwrap(),
            });
        })
    };
    let set_date_all = {
        let date_filters = date_filters.clone();
        Callback::from(move |_: _| {
            date_filters.set(LogDateFilter {
                date_after: NaiveDate::from_ymd_opt(2000, 1, 1).unwrap(),
                date_before: NaiveDate::from_ymd_opt(2100, 12, 31).unwrap(),
            });
        })
    };
    let change_nm = {
        let mode_filters = mode_filters.clone();
        Callback::from(move |ev: Event| {
            let target: Option<web_sys::EventTarget> = ev.target();
            let input = target.and_then(|t| t.dyn_into::<web_sys::HtmlInputElement>().ok());
            if let Some(input) = input {
                mode_filters.set(LogModeFilter {
                    allow_nm: input.checked(),
                    allow_cm: mode_filters.allow_cm,
                    allow_emboldened: true,
                });
            }
        })
    };
    let change_cm = {
        let mode_filters = mode_filters.clone();
        Callback::from(move |ev: Event| {
            let target: Option<web_sys::EventTarget> = ev.target();
            let input = target.and_then(|t| t.dyn_into::<web_sys::HtmlInputElement>().ok());
            if let Some(input) = input {
                mode_filters.set(LogModeFilter {
                    allow_cm: input.checked(),
                    allow_nm: mode_filters.allow_nm,
                    allow_emboldened: true,
                });
            }
        })
    };
    return html! {
    <>
        <BossTitle boss={selected_boss.as_ref().cloned()} />
        <div>
            <BossesNav bosses={gw2_data::get_encounters()} on_click={on_boss_select.clone()} current_boss={selected_boss.clone()}/>
            <br/>
        </div>
        <div id="content">
            <div id="left-col">
                <div id="filters" class="border-box">
                    {"Filter:"}
                    <div id="date_filter" class="border-box">
                        {"Date:"}<br/>
                        <span class="date-filter-label">{"From"}</span>
                        <input id="filter_after" type="date" name="filter_after" value={date_filters.date_after.format("%Y-%m-%d").to_string()} onchange={date_after_change}/><br/>
                        <span class="date-filter-label">{"To"}</span>
                        <input id="filter_before" type="date" name="filter_before" value={date_filters.date_before.format("%Y-%m-%d").to_string()} onchange={date_before_change}/><br/>
                        <ButtonGroup>
                        <Button text="2021" onclick={set_date_2021} outline={true} size={ButtonSize::Small}></Button>
                        <Button text="2022" onclick={set_date_2022} outline={true} size={ButtonSize::Small}></Button>
                        </ButtonGroup>
                        <ButtonGroup>
                        <Button text="2023" onclick={set_date_2023} outline={true} size={ButtonSize::Small}></Button>
                        <Button text="2024" onclick={set_date_2024} outline={true} size={ButtonSize::Small}></Button>
                        </ButtonGroup>
                        <ButtonGroup>
                        <Button text="PoF" onclick={set_date_pof} outline={true} size={ButtonSize::Small}></Button>
                        <Button text="EoD" onclick={set_date_eod} outline={true} size={ButtonSize::Small}></Button>
                        <Button text="SoTo" onclick={set_date_soto} outline={true} size={ButtonSize::Small}></Button>
                        </ButtonGroup>
                        <ButtonGroup>
                        <Button text="JW" onclick={set_date_jw} outline={true} size={ButtonSize::Small}></Button>
                        <Button text="All" onclick={set_date_all} outline={true} size={ButtonSize::Small}></Button>
                        </ButtonGroup>
                    </div>
                    <ButtonGroup>
                        <input type="checkbox" id="ckbCM" class="btn-check" name="CM" value="CM" checked={mode_filters.allow_cm} onchange={change_cm}/><label class="btn btn-outline-primary btn-sm" for="ckbCM">{"CM"}</label>
                        <input type="checkbox" id="ckbNM" class="btn-check" name="NM" value="NM" checked={mode_filters.allow_nm} onchange={change_nm}/><label class="btn btn-outline-primary btn-sm" for="ckbNM">{"None-CM"}</label>
                    </ButtonGroup>
                    <div>
                        {"Builds:"}
                        if cfg!(debug_assertions) {
                            <BuildFilter filterer_builds={filter_builds}/>
                        } else {
                            <br/>
                            { "Coming soon ..." }
                        }
                    </div>
                </div>
            </div>
            <div id="main-content">
                {for details}
            </div>
        </div>
    </>
    };
}

#[derive(Properties, PartialEq)]
struct BossTitleProps {
    boss: Option<GW2Encounter>,
}

#[function_component(BossTitle)]
fn title(BossTitleProps { boss }: &BossTitleProps) -> Html {
    let boss = boss.clone();

    html! {
        <div id="header">
            <h2>
                {match boss {
                    None => {
                        html !{"Boss overview"}
                    }
                    Some(boss) => html !{<>
                        <img id="boss-img" title={boss.name.clone()}
                        src={format!("/img/boss/{}.png", boss.name_id)} alt= {boss.name_id.clone()} />
                        { boss.name.clone() }</>
                    }
                }}
            </h2>
        </div>
    }
}

#[derive(Properties, PartialEq)]
struct BossDetailsLoadingProps {
    log_counts: UseStateHandle<LogCountData>,
}

#[function_component(BossDetailsLoading)]
fn boss_details_loading(BossDetailsLoadingProps { log_counts }: &BossDetailsLoadingProps) -> Html {
    let msg: String;
    if log_counts.logs_total == 0 {
        msg = String::from("Loading logs db");
    } else if log_counts.logs_processed != log_counts.logs_total {
        msg = format!(
            "Processing logs ... ({}/{})",
            log_counts.logs_processed, log_counts.logs_total
        );
    } else {
        msg = format!("Loaded {} logs", log_counts.logs_loaded);
    }
    return html! {
        <>{msg}</>
    };
}

#[derive(Properties, PartialEq)]
pub struct BossesProps {
    pub bosses: Vec<GW2Encounter>,
    pub current_boss: UseStateHandle<Option<GW2Encounter>>,
    pub on_click: Callback<GW2Encounter>,
    #[prop_or_default]
    pub push_path: bool,
}

#[function_component(BossesNav)]
pub fn bosses_nav(
    BossesProps {
        bosses,
        on_click,
        current_boss,
        push_path,
    }: &BossesProps,
) -> Html {
    let on_click = on_click.clone();
    let navigator = use_navigator().unwrap();
    bosses
        .iter()
        .map(|boss| {
            let on_boss_select = {
                let on_click = on_click.clone();
                let boss = boss.clone();
                let navigator = navigator.clone();
                let push_path = *push_path;
                Callback::from(move |_: _| {
                    let nav = navigator.clone();
                    if push_path {
                        nav.push(&Route::Boss {
                            boss_id: boss.name_id.clone(),
                        });
                    }
                    on_click.emit(boss.clone())
                })
            };
            let class = if current_boss.as_ref().is_some() && current_boss.as_ref().unwrap() == boss
            {
                classes!("boss-nav-img-current")
            } else {
                classes!("boss-nav-img")
            };
            html! {
                <img class={class} onclick={on_boss_select} title={boss.name.clone()} style={format!("background: {}",WING_COLORS[boss.wing as usize])}
                    src={format!("/img/boss/{}.png", boss.name_id)} alt= {boss.name_id.clone()} />
            }
        })
        .collect()
}

#[derive(Properties, PartialEq)]
pub struct BossesProps2 {
    pub bosses: Vec<GW2Encounter>,
    pub current_boss: Option<GW2Encounter>,
    pub on_click: Callback<GW2Encounter>,
    #[prop_or_default]
    pub push_path: bool,
}

#[function_component(BossesNav2)]
pub fn bosses_nav2(
    BossesProps2 {
        bosses,
        on_click,
        current_boss,
        push_path,
    }: &BossesProps2,
) -> Html {
    let on_click = on_click.clone();
    let navigator = use_navigator().unwrap();
    bosses
        .iter()
        .map(|boss| {
            let on_boss_select = {
                let on_click = on_click.clone();
                let boss = boss.clone();
                let navigator = navigator.clone();
                let push_path = *push_path;
                Callback::from(move |_: _| {
                    let nav = navigator.clone();
                    if push_path {
                        nav.push(&Route::Boss {
                            boss_id: boss.name_id.clone(),
                        });
                    }
                    on_click.emit(boss.clone())
                })
            };
            let class = if current_boss.as_ref().is_some() && current_boss.as_ref().unwrap() == boss
            {
                classes!("boss-nav-img-current")
            } else {
                classes!("boss-nav-img")
            };
            html! {
                <img class={class} onclick={on_boss_select} title={boss.name.clone()} style={format!("background: {}",WING_COLORS[boss.wing as usize])}
                    src={format!("/img/boss/{}.png", boss.name_id)} alt= {boss.name_id.clone()} />
            }
        })
        .collect()
}
