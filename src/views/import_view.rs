use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fmt::{Display, Formatter};

use gloo_console::{error, log, warn};
use gloo_file::{Blob, ObjectUrl};
use gloo_net::http::Request;
use gloo_utils::document;
use wasm_bindgen::JsCast;
use web_sys::HtmlElement;
use yew::html::Scope;
use yew::prelude::*;
use yew_bootstrap::component::Button;

use crate::builds::BuildGuesser;
use crate::components::check_box::CheckBox;
use crate::indexed_db_binding::IdbHandle;
use crate::short_log_data::{generate_short_log_data, ShortLogData};
use crate::views::import_view::GetEIError::{DPSReportError, GlooNetError, SerdeError};
use crate::views::import_view::ImportState::Initial;
use elite_insight_json::dps_report_api::DPSReportUploads;
use elite_insight_json::ei_dto::ei_data_types::{EliteInsightData, EliteInsightOrDPSReportError};
use elite_insight_json::log_manager::{LMEncounter, LMEncounterResult, LogManagerCache};

pub struct ImportView {
    total_logs: u32,
    processed_logs: u32,
    imported_logs: u32,
    state: ImportState,
    //dps_report_user_token: String
    import_error_text: String,
}

pub struct ImportProgressDetail {
    total_logs: u32,
    processed_logs: u32,
    imported_logs: u32,
}

pub struct ImportDoneDetail {
    total_logs: u32,
    imported_logs: u32,
}

pub enum ImportViewMessage {
    ImportStart,
    ImportProgress(ImportProgressDetail),
    ImportDone(ImportDoneDetail),
    ImportErrorMessage(String),
    ExportDataMessage(HashMap<String, ShortLogData>),
}

#[derive(PartialEq)]
pub enum ImportState {
    Initial,
    Importing,
    Imported,
}

#[derive(Properties, PartialEq)]
pub struct ImportViewProps {
    // reprocess bools
    #[prop_or_default]
    hotrp: bool,
    #[prop_or_default]
    w1rp: bool,
    #[prop_or_default]
    w2rp: bool,
    #[prop_or_default]
    w3rp: bool,
    #[prop_or_default]
    w4rp: bool,
    #[prop_or_default]
    w5rp: bool,
    #[prop_or_default]
    w6rp: bool,
    #[prop_or_default]
    w7rp: bool,
}

async fn import_logs(
    indexed_db: &IdbHandle,
    urls: &[String],
    link: Scope<ImportView>,
    reprocess: bool,
) {
    let build_guesser = BuildGuesser::from_db().await;
    let total = urls.len() as u32;
    let mut processed = 0;
    let mut imported = 0;
    for log_url in urls {
        link.send_message(ImportViewMessage::ImportProgress(ImportProgressDetail {
            total_logs: total,
            processed_logs: processed,
            imported_logs: imported,
        }));
        if !(indexed_db.has_log_data(log_url).await) || reprocess {
            match import_to_idb(indexed_db, log_url, &build_guesser).await {
                Ok(true) => imported += 1,
                Ok(false) => (),
                Err(err_msg) => link.send_message(ImportViewMessage::ImportErrorMessage(err_msg)),
            }
        }
        processed += 1;
    }
    link.send_message(ImportViewMessage::ImportDone(ImportDoneDetail {
        total_logs: total,
        imported_logs: imported,
    }));
}

impl ImportView {
    fn import_db_file_callback(link: Scope<Self>) -> Callback<MouseEvent> {
        Callback::from(move |_| {
            let link = link.clone();
            let file_input = document().get_element_by_id("fileimportdb").unwrap();
            let file_input = file_input.dyn_into::<web_sys::HtmlInputElement>().unwrap();
            let file = match file_input.files() {
                None => return,
                Some(files) => match files.get(0) {
                    None => return,
                    Some(file) => file,
                },
            };
            link.send_message(ImportViewMessage::ImportStart);
            wasm_bindgen_futures::spawn_local(async move {
                let file_ws = file;
                let file_gloo = gloo_file::File::from(file_ws);
                let string = gloo_file::futures::read_as_text(&file_gloo).await.unwrap();
                let logs: HashMap<String, ShortLogData> = match serde_json::from_str(&string) {
                    Ok(value) => value,
                    Err(err) => {
                        link.send_message(ImportViewMessage::ImportErrorMessage(format!(
                            "Database file is invalid or outdated: {err}"
                        )));
                        link.send_message(ImportViewMessage::ImportDone(ImportDoneDetail {
                            total_logs: 0,
                            imported_logs: 0,
                        }));
                        warn!(err.to_string());
                        return;
                    }
                };
                let indexed_db = IdbHandle::open().await;
                let total = logs.len() as u32;
                let mut imported = 0;
                let reprocess = false;
                for (processed, (log_url, data)) in logs.iter().enumerate() {
                    link.send_message(ImportViewMessage::ImportProgress(ImportProgressDetail {
                        total_logs: total,
                        processed_logs: processed as u32,
                        imported_logs: imported,
                    }));
                    if !(indexed_db.has_log_data(&log_url).await) || reprocess {
                        indexed_db
                            .add_log_data(&log_url, &data)
                            .expect("Failed to add log data");
                        imported += 1;
                    }
                }
                link.send_message(ImportViewMessage::ImportDone(ImportDoneDetail {
                    total_logs: total,
                    imported_logs: imported,
                }));
            });
        })
    }
}

impl Component for ImportView {
    type Message = ImportViewMessage;
    type Properties = ImportViewProps;

    fn create(_ctx: &Context<Self>) -> Self {
        ImportView {
            total_logs: 0,
            processed_logs: 0,
            imported_logs: 0,
            state: Initial,
            //dps_report_user_token: "".to_string()
            import_error_text: String::default(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            ImportViewMessage::ImportStart => {
                self.import_error_text = "".to_string();
                self.total_logs = 0;
                self.state = ImportState::Importing;
                true
            }
            ImportViewMessage::ImportProgress(details) => {
                self.total_logs = details.total_logs;
                self.imported_logs = details.imported_logs;
                self.processed_logs = details.processed_logs;
                true
            }
            ImportViewMessage::ImportDone(details) => {
                self.total_logs = details.total_logs;
                self.processed_logs = details.total_logs;
                self.imported_logs = details.imported_logs;
                self.state = ImportState::Imported;
                true
            }
            ImportViewMessage::ImportErrorMessage(msg) => {
                self.import_error_text = format!("{}{}\n", self.import_error_text, msg);
                false // update with other msg anyways
            }
            ImportViewMessage::ExportDataMessage(data) => {
                let a = document().create_element("a").unwrap();
                let blob_data = serde_json::to_string(&data).unwrap();
                let blob = Blob::new_with_options(blob_data.as_str(), Some("application/json"));
                let blob_url = ObjectUrl::from(blob);
                a.set_attribute("href", blob_url.as_ref()).unwrap();
                a.set_attribute("download", "logs.json").unwrap();
                let a_html_element: HtmlElement = a.dyn_into().unwrap();
                a_html_element.click();
                false
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        const SUCCESS_ONLY: bool = true;
        let link = ctx.link().clone();
        let link2 = link.clone();
        let link3 = link.clone();
        let link4 = link.clone();
        let link5 = link.clone();
        let link6 = link.clone();
        let import_lmc_callback = Callback::from(move |_| {
            let link: Scope<ImportView> = link.clone();
            let file_input = document().get_element_by_id("filelogmanagerdb").unwrap();
            let file_input = file_input.dyn_into::<web_sys::HtmlInputElement>().unwrap();
            let file = match file_input.files() {
                None => return,
                Some(files) => match files.get(0) {
                    None => return,
                    Some(file) => file,
                },
            };
            link.send_message(ImportViewMessage::ImportStart);
            wasm_bindgen_futures::spawn_local(async move {
                let file_ws = file;
                let file_gloo = gloo_file::File::from(file_ws);
                let string = gloo_file::futures::read_as_text(&file_gloo)
                    .await
                    .expect("Failed to read file");
                let indexed_db = IdbHandle::open().await;
                let mut logs_to_load = vec![];
                let lmc: LogManagerCache = match serde_json::from_str(&string) {
                    Ok(lmc) => lmc,
                    Err(err) => {
                        warn!(err.to_string());
                        link.send_message(ImportViewMessage::ImportErrorMessage(format!(
                            "Invalid Log Manager Data File: {err}"
                        )));
                        link.send_message(ImportViewMessage::ImportDone(ImportDoneDetail {
                            total_logs: 0,
                            imported_logs: 0,
                        }));
                        return;
                    }
                };

                let encounter_ids: HashSet<LMEncounter> =
                    elite_insight_json::gw2_data::get_encounters()
                        .into_iter()
                        .map(|gw2e| gw2e.id)
                        .collect();
                for filename in lmc.logs_by_filename.keys() {
                    let lml = lmc.logs_by_filename.get(filename).unwrap();
                    if encounter_ids.contains(&lml.encounter)
                        && lml.dps_report_ei_upload.url.is_some()
                        && (lml.encounter_result == LMEncounterResult::Success || !SUCCESS_ONLY)
                    {
                        // todo unwrap
                        logs_to_load.push(lml.dps_report_ei_upload.url.as_ref().unwrap().clone());
                    }
                }
                import_logs(&indexed_db, &logs_to_load, link, false).await;
            });
        });
        let import_dps_report_callback = Callback::from(move |_| {
            let link = link2.clone();
            let user_token = document()
                .get_element_by_id("txtUserToken")
                .unwrap()
                .dyn_into::<web_sys::HtmlInputElement>()
                .unwrap()
                .value();
            link.send_message(ImportViewMessage::ImportStart);
            wasm_bindgen_futures::spawn_local(async move {
                let mut urls = match get_dps_report_uploads(user_token).await {
                    Ok(urls) => urls,
                    Err(err) => {
                        link.send_message(ImportViewMessage::ImportErrorMessage(format!(
                            "Failed to get uploads from dps.report: {err}"
                        )));
                        link.send_message(ImportViewMessage::ImportDone(ImportDoneDetail {
                            total_logs: 0,
                            imported_logs: 0,
                        }));
                        warn!(err);
                        return;
                    }
                };
                let encounter_sids: HashSet<String> =
                    elite_insight_json::gw2_data::get_encounters()
                        .into_iter()
                        .map(|gw2e| gw2e.name_id)
                        .collect();
                urls = urls
                    .into_iter()
                    .filter(|url| encounter_sids.contains(url.split("_").last().unwrap()))
                    .collect();
                let indexed_db = IdbHandle::open().await;
                import_logs(&indexed_db, &urls, link, false).await;
            })
        });
        let reprocessing_callback = Callback::from(move |_| {
            let link = link3.clone();
            link.send_message(ImportViewMessage::ImportStart);
            wasm_bindgen_futures::spawn_local(async move {
                let indexed_db = IdbHandle::open().await;
                let mut all_urls = Vec::new();
                for encounter in elite_insight_json::gw2_data::get_encounters() {
                    let incl = match encounter.wing {
                        //1 => true,
                        //2 => true,
                        //3 => true,
                        //4 => true,
                        //5 => true,
                        //6 => true,
                        //7 => true,
                        //8 => true,
                        9 => true,
                        _ => false,
                    };
                    if !incl {
                        continue;
                    }
                    let mut urls = indexed_db
                        .get_logs_for_boss::<ShortLogData, String>(&encounter.name_id, |key, _| {
                            format!("https://dps.report/{}", key)
                        })
                        .await
                        .unwrap_or(Vec::new());
                    all_urls.append(&mut urls);
                }
                import_logs(&indexed_db, &all_urls, link, true).await;
            })
        });
        let import_links_callback = Callback::from(move |_| {
            let link = link4.clone();
            link.send_message(ImportViewMessage::ImportStart);
            let links_text: String = document()
                .get_element_by_id("txtImportLinks")
                .unwrap()
                .dyn_into::<web_sys::HtmlTextAreaElement>()
                .unwrap()
                .value();
            wasm_bindgen_futures::spawn_local(async move {
                let mut urls: Vec<String> = links_text
                    .split("\n")
                    .map(|s| s.trim().to_string())
                    .collect();
                let encounter_sids: HashSet<String> =
                    elite_insight_json::gw2_data::get_encounters()
                        .into_iter()
                        .map(|gw2e| gw2e.name_id)
                        .collect();
                urls.retain(|url| encounter_sids.contains(url.split("_").last().unwrap()));

                let indexed_db = IdbHandle::open().await;
                import_logs(&indexed_db, &urls, link, false).await;
            })
        });
        let import_file_callback = Self::import_db_file_callback(link5);
        let export_callback = Callback::from(move |_| {
            let link = link6.clone();
            link.send_future(async {
                let db = IdbHandle::open().await;
                let data = db.get_all_logs_data().await.unwrap();
                ImportViewMessage::ExportDataMessage(data)
            });
        });
        let hot_callback = Callback::from(move |(v, id): (bool, String)| {
            log!("hot", id, v);
        });
        /*let pof_callback = Callback::from(move |(v, id): (bool, String)| {
            log!("pof", id, v);
        });*/
        let error_rows: Vec<Html> = self
            .import_error_text
            .split("\n")
            .map(|part| html! {<div> {part} </div>})
            .collect();
        html! {
        <div class="overflow-auto">
        <div class="container">
        <div class="row g-3 mt-3">
            <div class="col-6">
            <div class="border bg-body-secondary p-3 rounded">
                <h5>{"Import from links"}</h5>
                <div class=" mb-3">
                    <label for="txtImportLinks" class="form-label">{"Use dps.report upload link to import"}</label>
                    <textarea class="form-control" id="txtImportLinks" placeholder="https://dps.report/Abcd-20200102-123456_vg\nhttps://dps.report/dEfg-20210304-234506_gors" rows="3"></textarea>
                </div>
                <Button text={"Import"} onclick={import_links_callback} disabled={self.state == ImportState::Importing}></Button>
            </div>
            </div>
            <div class="col-6">
            <div class="border bg-body-secondary p-3 rounded">
                <h5>{"Import from arcdps Log Manager"}</h5>
                <label for="filelogmanagerdb" class="form-label">{"The "} <a href="https://gw2scratch.com/tools/manager">{"arcdps Log Manager"}</a>{" data base file is usually stored at C:\\Users\\<username>\\AppData\\Local\\ArcdpsLogManager\\LogDataCache.json"}</label>
                <div class="input-group mb-3">
                    <span class="input-group-text">{"Log Manager Data File"}</span>
                    <input class="form-control" type="file" id="filelogmanagerdb" aria-label="Log Manager File"/>
                </div>
                <Button text={"Import"} onclick={import_lmc_callback} disabled={self.state == ImportState::Importing}></Button>
            </div>
            </div>
            <div class="col-6">
            <div class="border bg-body-secondary p-3 rounded">
                <h5>{"Import dps.report uploads"}</h5>
                <label for="txtUserToken" class="form-label">{"Visit "}<a href={"https://dps.report/getUserToken"} target={"_blank"}>{"https://dps.report/getUserToken"}</a>{" to find out your user token."}</label>
                <div class="input-group mb-3">
                    <span class="input-group-text">{"User token"}</span>
                    <input class="form-control" type="text" id="txtUserToken" aria-label="User Token" value="j4feo8ki9qg3dgdhi2r2c43dqnmfra8q"/>
                </div>
                <br/>
                <Button text={"Import"} onclick={import_dps_report_callback} disabled={self.state == ImportState::Importing}></Button>
            </div>
            </div>
            <div class="col-6">
            <div class="border bg-body-secondary p-3 rounded">
                <h5>{"Import database"}</h5>
                <label for="fileimportdb" class="form-label">{"An exported database JSON file can be imported here."}</label>
                <div class="input-group mb-3">
                    <span class="input-group-text">{"Database File"}</span>
                    <input class="form-control" type="file" id="fileimportdb" aria-label="Database File"/>
                </div>
                <Button text={"Import"} onclick={import_file_callback} disabled={self.state == ImportState::Importing}></Button>
            </div>
            </div>
            <div class="col-6">
            <div class="border bg-body-secondary p-3 rounded">
                <h5>{"Export database"}</h5>
                <Button text={"Export"} onclick={export_callback} disabled={self.state == ImportState::Importing}></Button>
            </div>
            </div>
            <div class="col-6">
            <div class="border bg-body-secondary p-3 rounded">
                <h5>{"Reprocess logs"}</h5>
                if cfg!(debug_assertions) {
                <div class="input-group mb-1 no-select">
                    <div class="input-group-text">
                        <CheckBox  classes={classes!("mt-0")} id="chkReProcHoT" checked={ctx.props().hotrp} on_change={hot_callback.clone()}/>
                    </div>
                    <label class="input-group-text bg-body-secondary" for="chkReProcHoT">{"HoT Raids"}</label>
                    <div class="input-group-text">
                        <CheckBox  classes={classes!("mt-0")} id="chkReProcW1" checked={ctx.props().w1rp} on_change={hot_callback.clone()}/>
                        <label class="ms-1" for="chkReProcW1">{"W1"}</label>
                    </div>
                    <div class="input-group-text">
                        <CheckBox classes={classes!("mt-0")} id="chkReProcW2" checked={ctx.props().w2rp} on_change={hot_callback.clone()}/>
                        <label class="ms-1" for="chkReProcW2">{"W2"}</label>
                    </div>
                    <div class="input-group-text">
                        <CheckBox classes={classes!("mt-0")} id="chkReProcW3" checked={ctx.props().w3rp} on_change={hot_callback.clone()}/>
                        <label class="ms-1" for="chkReProcW3">{"W3"}</label>
                    </div>
                    <div class="input-group-text">
                        <CheckBox classes={classes!("mt-0")} id="chkReProcW4" checked={ctx.props().w4rp} on_change={hot_callback}/>
                        <label class="ms-1" for="chkReProcW4">{"W4"}</label>
                    </div>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-text">
                        <input class="form-check-input mt-0" type="checkbox" id="chkReProcPoF"/>
                    </div>
                    <label class="input-group-text bg-body-secondary" for="chkReProcPoF">{"PoF Raids"}</label>
                    <div class="input-group-text">
                        <input class="form-check-input mt-0" type="checkbox" id="chkReProcW5"/>
                        <label class="ms-1" for="chkReProcW5">{"W5"}</label>
                    </div>
                    <div class="input-group-text">
                        <input class="form-check-input mt-0" type="checkbox" id="chkReProcW6"/>
                        <label class="ms-1" for="chkReProcW6">{"W6"}</label>
                    </div>
                    <div class="input-group-text">
                        <input class="form-check-input mt-0" type="checkbox" id="chkReProcW7"/>
                        <label class="ms-1" for="chkReProcW7">{"W7"}</label>
                    </div>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-text">
                        <input class="form-check-input mt-0" type="checkbox" id="chkReProcIBS"/>
                    </div>
                    <label class="input-group-text bg-body-secondary" for="chkReProcIBS">{"IBS Strikes"}</label>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-text">
                        <input class="form-check-input" type="checkbox" id="chkReProcEoD"/>
                    </div>
                    <label class="input-group-text bg-body-secondary" for="chkReProcEoD">{"EoD Strikes"}</label>
                </div>
                <Button text={"Start"} onclick={reprocessing_callback} disabled={self.state == ImportState::Importing}></Button>
                } else {
                    {"Coming soon... "}
                }
            </div>
            </div>
            <div>
            <div class="border bg-body-secondary p-3 rounded">
                if self.state == ImportState::Initial {
                    {"Ready"}<br/><br/>
                    <progress value="0"/>
                } else if self.state == ImportState::Importing {
                    if self.total_logs > 0  {
                        {format!("Processing log {}/{} ...", self.processed_logs + 1, self.total_logs)}<br/>
                        {format!("Imported: {} ", self.imported_logs)}<br/>
                        <progress value={self.processed_logs.to_string()} max={self.total_logs.to_string()}/>
                    } else {
                        {"Reading data..."}<br/><br/>
                        <progress />
                    }
                } else if self.state == ImportState::Imported {
                        {format!("Processed {} logs", self.processed_logs)}<br/>
                        {format!("Imported: {} ", self.imported_logs)}<br/>
                        <progress value={self.processed_logs.to_string()} max={self.total_logs.to_string()}/>
                }
                if error_rows.len() > 1 {
                    <div class="alert-danger alert max-vh-25 overflow-auto">
                        {for error_rows}
                    </div>
                }
            </div>
            </div>
        </div>
        </div>
        </div>
        }
    }
}

async fn import_to_idb(
    indexed_db: &IdbHandle,
    log_url: &str,
    build_guesser: &BuildGuesser,
) -> Result<bool, String> {
    let ei_data = get_ei_data(log_url).await;
    match ei_data {
        Ok(ei_data) => {
            if ei_data.success() {
                let boss_id = log_url.split("_").last().unwrap();
                let sld = generate_short_log_data(&ei_data, boss_id, build_guesser);
                indexed_db
                    .add_log_data(log_url, &sld)
                    .map_err(|err| err.to_string())?;
                Ok(true)
            } else {
                Ok(false)
            }
        }
        Err(err) => {
            error!("Could not add ", log_url, err.to_string());
            Err(format!("Could not add {}: {}", log_url, err))
        }
    }
}

impl Error for GetEIError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

#[derive(Debug)]
pub enum GetEIError {
    GlooNetError(gloo_net::Error),
    SerdeError(serde_json::Error),
    DPSReportError(elite_insight_json::ei_dto::ei_data_types::DPSReportError),
}

impl Display for GetEIError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            GlooNetError(err) => {
                write!(f, "{err}")
            }
            SerdeError(err) => {
                write!(f, "{err}")
            }
            DPSReportError(err) => {
                write!(f, "{}", err.error)
            }
        }
    }
}

pub async fn get_ei_data(dps_report_url: &str) -> Result<EliteInsightData, GetEIError> {
    let mut url;
    if cfg!(debug_assertions) {
        // use local server for debugging
        url = "/logs/getJson/".to_owned();
        url.push_str(&dps_report_url[19..]);
    } else {
        url = "https://dps.report/getJson?permalink=".to_owned();
        url.push_str(&dps_report_url[19..]);
    }
    log!("downloading", &url);
    let req = Request::get(&url).send().await;
    match req {
        Ok(resp) => {
            let data = resp.text().await.unwrap();
            let parsed = serde_json::from_str::<EliteInsightOrDPSReportError>(&data);
            match parsed {
                Ok(response) => match response {
                    EliteInsightOrDPSReportError::Data(ei_data) => Ok(ei_data),
                    EliteInsightOrDPSReportError::Error(err) => {
                        Err(GetEIError::DPSReportError(err))
                    }
                },
                Err(err) => Err(SerdeError(
                    match serde_json::from_str::<EliteInsightData>(&data) {
                        Err(error) => error,
                        Ok(_) => err,
                    },
                )),
            }
        }
        Err(err) => {
            error!("Could not get data", err.to_string());
            Err(GlooNetError(err))
        }
    }
}

async fn get_dps_report_uploads(user_token: String) -> Result<Vec<String>, String> {
    // this success only
    let mut pages = 1;
    let mut page = 1;
    let mut urls = Vec::new();
    while page <= pages {
        let url = format!(
            "https://dps.report/getUploads?page={}&userToken={}&perPage=1000",
            page, user_token
        );
        let req = Request::get(&url).send().await;
        match req {
            Ok(resp) => {
                let data = resp.text().await.unwrap();
                let parsed = serde_json::from_str::<DPSReportUploads>(&data);
                match parsed {
                    Ok(data) => {
                        pages = data.pages;
                        for upload in data.uploads {
                            if upload.encounter.success {
                                urls.push(upload.permalink);
                            }
                        }
                    }
                    Err(err) => return Err(err.to_string()),
                }
            }
            Err(err) => return Err(err.to_string()),
        }
        page += 1;
    }
    Ok(urls)
}
