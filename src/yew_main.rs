use std::string::String;
use yew::prelude::*;
use yew_bootstrap::icons::BIFiles;
use yew_bootstrap::util::{include_cdn_js, include_inline};
use yew_router::prelude::*;

use crate::components::nav_header::NavHeader;
use crate::views::boss_view::BossView;
use crate::views::build_view::BuildView;
use crate::views::builds_view::BuildsView;
use crate::views::import_view::ImportView;
use crate::views::timeline::Timeline;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/boss/:boss_id")]
    Boss { boss_id: String },
    #[at("/builds")]
    Builds,
    #[at("/build/:build_hash")]
    Build { build_hash: u64 },
    #[at("/timeline")]
    Timeline,
    #[at("/import")]
    Import,
    #[not_found]
    #[at("/404")]
    NotFound,
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <>
        {include_inline()}
        {BIFiles::cdn()}
        <BrowserRouter>
        <NavHeader/>
            <Switch<Route> render={switch} />
        </BrowserRouter>
        { include_cdn_js() }
        </>
    }
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <BossView boss_id={""}/> },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
        Route::Boss { boss_id } => {
            html! { <BossView boss_id={boss_id}/> }
        }
        Route::Builds => html! {<BuildsView/>},
        Route::Build { build_hash } => {
            html! {<BuildView build_hash={build_hash}/>}
        }
        Route::Timeline => html! {<Timeline/>},
        Route::Import => html! { <ImportView/> },
    }
}

pub fn yew_main() {
    yew::Renderer::<App>::new().render();
}

pub const WING_COLORS: [&str; 14] = [
    "black", "#DE5B49", "#E37B40", "#F0CA4D", "#347F1F", "#41a7b0", "#2A42A9", "#4F0080",
    "#42f7ff", "#55a10a", "#d3a047", "gray", "gray", "gray",
];
