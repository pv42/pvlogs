use crate::components::{
    boss_details::get_color_from_gradient,
    table_v2::{TableColAlignment, TableHeaderEntry, TableV2},
};
use yew::{function_component, html, Html, Properties};

#[derive(Debug, Clone, PartialEq)]
pub struct BoonTableRowData {
    pub boss_name: String,
    pub quickness1: f64,
    pub quickness2: f64,
    pub alac1: f64,
    pub alac2: f64,
    pub might1: f64,
    pub might2: f64,
}

fn bt_row(
    name: String,
    f: Box<dyn Fn(&BoonTableRowData) -> Html>,
    g: Box<dyn Fn(&BoonTableRowData) -> String>,
) -> TableHeaderEntry<BoonTableRowData> {
    TableHeaderEntry::<BoonTableRowData> {
        name,
        content: f,
        is_sortable: false,
        width: Some(80),
        min_width: Some(40),
        bg_color: Some(g),
        alignment: TableColAlignment::AlignCenter,
        class: None,
        ..Default::default()
    }
}

#[derive(Debug, Clone, Properties, PartialEq)]
pub struct BoonTableProps {
    pub data: Vec<BoonTableRowData>,
}

#[function_component(BoonTable)]
pub fn boon_table(BoonTableProps { data }: &BoonTableProps) -> Html {
    let cols = vec![
        TableHeaderEntry::<BoonTableRowData> {
            name: "".to_owned(),
            content: Box::new(|row| html! { row.boss_name.clone() }),
            is_sortable: false,
            width: Some(40),
            min_width: Some(40),
            bg_color: None,
            alignment: TableColAlignment::AlignStart,
            class: None,
            ..Default::default()
        },
        bt_row(
            "Quickness Sub1".to_owned(),
            Box::new(|row| html! { format!("{:.1}%", row.quickness1) }),
            Box::new(|value| get_color_from_gradient(value.quickness1, 50.0, 77.0, 98.0)),
        ),
        bt_row(
            "Quickness Sub2".to_owned(),
            Box::new(|row| html! { format!("{:.1}%", row.quickness2) }),
            Box::new(|value| get_color_from_gradient(value.quickness2, 50.0, 77.0, 98.0)),
        ),
        bt_row(
            "Alacrity Sub1".to_owned(),
            Box::new(|row| html! { format!("{:.1}%", row.alac1) }),
            Box::new(|value| get_color_from_gradient(value.alac1, 50.0, 77.0, 98.0)),
        ),
        bt_row(
            "Alacrity Sub2".to_owned(),
            Box::new(|row| html! { format!("{:.1}%", row.alac2) }),
            Box::new(|value| get_color_from_gradient(value.alac2, 50.0, 77.0, 98.0)),
        ),
        bt_row(
            "Might Sub1".to_owned(),
            Box::new(|row| html! { format!("{:.1}", row.might1) }),
            Box::new(|value| get_color_from_gradient(value.might1, 13.0, 19.0, 24.5)),
        ),
        bt_row(
            "Might Sub2".to_owned(),
            Box::new(|row| html! { format!("{:.1}", row.might2) }),
            Box::new(|value| get_color_from_gradient(value.might2, 13.0, 19.0, 24.5)),
        ),
    ];
    html! {
        <TableV2<BoonTableRowData> table_id="1" data={data.clone()} columns={cols}/>
    }
}
