use crate::yew_main::Route;
use gloo_console::log;
use gloo_storage::Storage;
use gloo_utils::document;
use std::collections::HashMap;
use yew::prelude::*;
use yew_router::components::Link;

use yew_bootstrap::component::{NavBar, NavDropdown};
use yew_router::hooks::use_location;
use yew_router::Routable;

/// # A singular dropdown item, child of [NavDropdown]
/// Used as a child of [NavDropdown] to create a dropdown menu.
///
/// See [NavDropdownOnclickItemProps] for a listing of properties.
pub struct NavDropdownOnclickItem {}

/// # Properties for [NavDropdownOnclickItem]
#[derive(Properties, Clone, PartialEq)]
pub struct NavDropdownOnclickItemProps {
    /// Link for the item
    #[prop_or_default]
    pub onclick: Callback<MouseEvent>,
    /// Wether the element is active
    #[prop_or_default]
    pub active: bool,
    /// Content of the item
    #[prop_or_default]
    pub children: Html,
}

impl Component for NavDropdownOnclickItem {
    type Message = ();
    type Properties = NavDropdownOnclickItemProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let mut class = classes!["dropdown-item"];
        if props.active {
            class.push("active")
        }
        html! {
            <li>
                <button class={class} onclick={props.onclick.clone()}>{props.children.clone()}</button>
            </li>
        }
    }
}

#[derive(Properties, PartialEq)]
struct NavRouterItemProps {
    to: Route,
    text: String,
}

#[function_component(NavRouterItem)]
fn nav_router_item(NavRouterItemProps { to, text }: &NavRouterItemProps) -> Html {
    let current = Route::from_path(use_location().unwrap().path(), &HashMap::new());
    let mut classes = vec!["nav-link"];
    if current.as_ref() == Some(to) {
        classes.push("active");
    }
    html! {
        <li class="nav-item">
            <Link<Route> classes={classes!(classes)} to={to.clone()}>{{text}}</Link<Route>>
        </li>
    }
}

#[function_component(NavHeader)]
pub fn nav_header() -> Html {
    let theme = use_state(|| {
        gloo_storage::LocalStorage::get::<String>("theme").unwrap_or("dark".to_owned())
    });
    let _ = gloo_storage::LocalStorage::set("theme", theme.as_str());
    log!("using theme", theme.as_str());
    document().document_element().map(|root| {
        let _ = root.set_attribute("data-bs-theme", &theme);
    });
    let set_dark_mode = {
        let theme = theme.clone();
        Callback::from(move |_: _| theme.set("dark".to_owned()))
    };
    let set_light_mode = {
        let theme = theme.clone();
        Callback::from(move |_: _| theme.set("light".to_owned()))
    };
    html!(
        <NavBar nav_id={"main-nav"} class="navbar-expand-lg navbar-dark bg-primary" expanded={true}>
            <NavRouterItem text="Bosses" to={Route::Home} />
            <NavRouterItem text="Builds" to={Route::Builds}/>
            <NavRouterItem text="Timeline" to={Route::Timeline}/>
            <NavRouterItem text="Import Logs" to={Route::Import}/>
            <li class="nav-item py-2 py-lg-1 col-12 col-lg-auto">
                <div class="vr d-none d-lg-flex h-100 mx-lg-2 text-white"/>
            </li>
            <NavDropdown text="Theme" id="nav-dropdown-darkmode">
                <NavDropdownOnclickItem active={theme.as_str() == "dark"} onclick={set_dark_mode}>{yew_bootstrap::icons::BI::MOON_STARS_FILL} {" Dark"}</NavDropdownOnclickItem>
                <NavDropdownOnclickItem active={theme.as_str() == "light"} onclick={set_light_mode}>{yew_bootstrap::icons::BI::BRIGHTNESS_HIGH_FILL} {" Light"}</NavDropdownOnclickItem>
            </NavDropdown>
        </NavBar>
    )
}
