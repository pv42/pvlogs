use crate::builds::PlayerBuildData;
use crate::components::boss_details::get_class_from_spec;
use crate::components::dist_viz::DistViz;
use crate::components::table_v2::TableHeaderEntry;
use crate::components::table_v2::{TableColAlignment, TableV2};
use crate::yew_main::Route;
use std::cmp::Ordering;
use std::collections::HashMap;
use yew::function_component;
use yew::Properties;
use yew::{html, Html};
use yew_router::prelude::Link;

#[derive(Properties, PartialEq)]
pub struct BuildTableProps {
    pub build_counts: HashMap<String, i32>,
    pub builds: HashMap<String, PlayerBuildData>,
    pub build_dps_sum: HashMap<String, i32>,
    pub build_dps_max: HashMap<String, i32>,
    pub build_cc_sum: HashMap<String, f64>,
    pub build_dps_data: HashMap<String, Vec<i32>>,
    pub build_uptime_sum: Option<HashMap<String, f64>>,
    pub total_max_dps: i32,
}

#[derive(Properties, PartialEq, Clone)]
struct BuildStats {
    count: i32,
    build: PlayerBuildData,
    dps_avg: i32,
    dps_max: i32,
    cc_avg: f64,
    dps_data: Vec<i32>,
    uptime_avg: Option<f64>,
    total_max_dps: i32,
    dps_of_benchamrk: Option<f64>,
}

#[function_component(BuildTable)]
pub fn build_table(
    BuildTableProps {
        build_counts,
        builds,
        build_dps_sum,
        build_dps_max,
        build_cc_sum,
        build_dps_data,
        total_max_dps,
        build_uptime_sum,
    }: &BuildTableProps,
) -> Html {
    let mut builds_stats = vec![];
    build_counts.iter().for_each(|(build_name, count)| {
        let build = &builds[build_name];
        let dps_data = build_dps_data[build_name].clone();
        builds_stats.push(BuildStats {
            count: *count,
            build: build.clone(),
            dps_avg: (build_dps_sum[build_name] + (count / 2)) / count,
            dps_max: build_dps_max[build_name],
            cc_avg: build_cc_sum[build_name] / (*count as f64),
            dps_data: dps_data.clone(),
            uptime_avg: build_uptime_sum
                .as_ref()
                .map(|uptimes| uptimes[build_name] / (*count as f64)),
            total_max_dps: *total_max_dps,
            dps_of_benchamrk: build.benchmark.map(|mark| {
                ((build_dps_sum[build_name] as f64) / *count as f64) / mark as f64 * 100.0
            }),
        });
    });
    let mut columns = vec![
        TableHeaderEntry::<BuildStats> {
            name: "Build".to_string(),
            content: Box::new(|row| {
                let prof_icon_url = format!("/img/{}_tango_icon_20px.png", &row.build.profession);
                html! {
                    <Link<Route> to={Route::Build {build_hash: row.build.hash}}>
                        <img src={prof_icon_url} alt={row.build.profession.clone()} class="small-icon"/> {&row.build.name.clone()}
                    </Link<Route>>
                }
            }),
            cmp_fn: Box::new(|a, b| a.build.profession.cmp(&b.build.profession)),
            is_sortable: true,
            alignment: TableColAlignment::AlignStart,
            ..Default::default()
        },
        TableHeaderEntry {
            name: "#".to_string(),
            content: Box::new(|row| html! {row.count}),
            cmp_fn: Box::new(|a, b| a.count.cmp(&b.count)),
            is_sortable: true,
            alignment: TableColAlignment::AlignEnd,
            ..Default::default()
        },
        TableHeaderEntry {
            name: "DPS".to_string(),
            content: Box::new(|row| html! {row.dps_avg}),
            cmp_fn: Box::new(|a, b| a.dps_avg.cmp(&b.dps_avg)),
            is_sortable: true,
            alignment: TableColAlignment::AlignEnd,
            ..Default::default()
        },
        TableHeaderEntry {
            name: "% of Benchmark".to_string(),
            content: Box::new(|row| {
                row.dps_of_benchamrk
                    .map(|of_mark| html! { format!("{of_mark:.1}%") })
                    .unwrap_or_default()
            }),
            cmp_fn: Box::new(|a, b| {
                a.dps_of_benchamrk
                    .partial_cmp(&b.dps_of_benchamrk)
                    .unwrap_or(Ordering::Equal)
            }),
            is_sortable: true,
            alignment: TableColAlignment::AlignEnd,
            ..Default::default()
        },
        TableHeaderEntry {
            name: "DPS VIZ".to_string(),
            content: Box::new(|row| {
                html! {
                        <DistViz data={row.dps_data.clone()} class_name={get_class_from_spec(&row.build.profession)} global_max_value={row.total_max_dps}/>
                }
            }),
            cmp_fn: Box::new(|a, b| a.dps_max.cmp(&b.dps_max)),
            is_sortable: true,
            alignment: TableColAlignment::AlignStart,
            ..Default::default()
        },
        TableHeaderEntry {
            name: "CC".to_string(),
            content: Box::new(|row| html! { format!("{:.0}", row.cc_avg) }),
            cmp_fn: Box::new(|a, b| a.cc_avg.total_cmp(&b.cc_avg)),
            is_sortable: true,
            alignment: TableColAlignment::AlignEnd,
            ..Default::default()
        },
    ];
    if build_uptime_sum.is_some() {
        columns.push(TableHeaderEntry {
            name: "Uptime".to_string(),
            content: Box::new(|row| html! { format!("{:.1}%", row.uptime_avg.unwrap()) }),
            cmp_fn: Box::new(|a, b| a.uptime_avg.unwrap().total_cmp(&b.uptime_avg.unwrap())),
            is_sortable: true,
            alignment: TableColAlignment::AlignEnd,
            ..Default::default()
        });
    }
    html! {
        <TableV2<BuildStats> columns={columns} data={builds_stats} table_id="dps_tbl"/>
    }
}
