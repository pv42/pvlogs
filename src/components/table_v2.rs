use std::cmp::Ordering;
use std::marker::PhantomData;
use yew::{classes, html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Copy, Clone, Debug)]
enum SortOrder {
    Ascending,
    Descending,
}

pub enum TableColAlignment {
    AlignStart,
    AlignCenter,
    AlignEnd,
}

impl TableColAlignment {
    fn as_class(&self) -> String {
        match self {
            TableColAlignment::AlignStart => "text-start".to_string(),
            TableColAlignment::AlignCenter => "text-center".to_string(),
            TableColAlignment::AlignEnd => "text-end".to_string(),
        }
    }
}

pub struct TableHeaderEntry<T> {
    pub name: String,
    pub content: Box<dyn Fn(&T) -> Html>,
    pub cmp_fn: Box<dyn Fn(&T, &T) -> Ordering>,
    pub is_sortable: bool,
    pub width: Option<u32>,
    pub min_width: Option<u32>,
    pub bg_color: Option<Box<dyn Fn(&T) -> String>>,
    pub alignment: TableColAlignment,
    pub class: Option<String>,
}

impl<T> Default for TableHeaderEntry<T> {
    fn default() -> Self {
        Self {
            name: Default::default(),
            content: Box::new(|_input| html!("missing content")),
            cmp_fn: Box::new(|_, _| Ordering::Equal),
            width: None,
            min_width: None,
            is_sortable: false,
            bg_color: None,
            alignment: TableColAlignment::AlignStart,
            class: None,
        }
    }
}

impl<T> TableHeaderEntry<T> {
    pub fn sortable(
        name: String,
        content: Box<dyn Fn(&T) -> Html>,
        cmp_fn: Box<dyn Fn(&T, &T) -> Ordering>,
    ) -> TableHeaderEntry<T> {
        TableHeaderEntry {
            name,
            content,
            cmp_fn,
            width: None,
            min_width: None,
            is_sortable: true,
            bg_color: None,
            alignment: TableColAlignment::AlignEnd,
            class: None,
        }
    }
    pub fn sortable_w(
        name: String,
        content: Box<dyn Fn(&T) -> Html>,
        cmp_fn: Box<dyn Fn(&T, &T) -> Ordering>,
        width: u32,
    ) -> TableHeaderEntry<T> {
        TableHeaderEntry {
            name,
            content,
            cmp_fn,
            width: Some(width),
            min_width: Some(width),
            is_sortable: true,
            bg_color: None,
            alignment: TableColAlignment::AlignEnd,
            class: None,
        }
    }
}

#[derive(Debug)]
pub struct TableV2Message {
    sort_index: usize,
    sort_order: SortOrder,
}

#[derive(Properties)]
pub struct TableV2Props<T> {
    pub columns: Vec<TableHeaderEntry<T>>,
    pub table_id: String,
    pub data: Vec<T>,
}

impl<T> PartialEq for TableV2Props<T> {
    fn eq(&self, other: &Self) -> bool {
        self.table_id == other.table_id
            && self.data.len() == other.data.len()
            && self.columns.len() == other.columns.len()
    }
}

pub struct TableV2<T: Clone> {
    current_sort_index: usize,
    current_sort_order: SortOrder,
    phantom: PhantomData<T>,
}

impl<T: 'static + Clone> Component for TableV2<T> {
    type Message = TableV2Message;
    type Properties = TableV2Props<T>;

    fn create(_ctx: &Context<Self>) -> Self {
        TableV2 {
            current_sort_index: 0,
            current_sort_order: SortOrder::Descending,
            phantom: PhantomData,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        self.current_sort_index = msg.sort_index;
        self.current_sort_order = msg.sort_order;
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let columns = &ctx.props().columns;
        let table_id = &ctx.props().table_id;
        let data = &ctx.props().data;
        let link = ctx.link().clone();
        let current_sort_index = self.current_sort_index;
        let current_sort_order = self.current_sort_order;
        let set_sort_index = Callback::from(move |column_index: usize| {
            if current_sort_index == column_index && current_sort_order == SortOrder::Ascending {
                link.send_message(TableV2Message {
                    sort_index: column_index,
                    sort_order: SortOrder::Descending,
                });
            } else {
                link.send_message(TableV2Message {
                    sort_index: column_index,
                    sort_order: SortOrder::Ascending,
                });
            }
        });
        let headers = columns.iter().enumerate().map(move |(index, column_data)| {
            let set_sort_index = set_sort_index.clone();
            let style = match (column_data.width , column_data.min_width) {
                (None,None) =>  "".to_string(),
                (Some(width),None) => format!("width:{width}px"),
                (None, Some(min_width),) => format!("min-width:{min_width}px"),
                (Some(width), Some(min_width)) => format!("width:{width}px;min-width:{min_width}px")
            };
            let aria_sort;
            if self.current_sort_index == index {
                match self.current_sort_order {
                    SortOrder::Ascending => { aria_sort = "ascending".to_string() }
                    SortOrder::Descending => { aria_sort = "descending".to_string() }
                }
            } else {
                aria_sort = "none".to_string()
            }
            let set_this_sort = Callback::from(move |_| { set_sort_index.emit(index) });
            html! {
            if column_data.is_sortable {
                <th onclick={set_this_sort} style={style} class={classes!(column_data.alignment.as_class())} aria-sort={aria_sort}>{column_data.name.clone()}</th>
            } else {
                <th style={style}>{column_data.name.clone()}</th>
            }

        }
        });
        let mut data_sorted = data.clone();
        if self.current_sort_order == SortOrder::Ascending {
            data_sorted.sort_by(&columns.get(self.current_sort_index).unwrap().cmp_fn);
        } else {
            data_sorted.sort_by(|a, b| {
                (columns.get(self.current_sort_index).unwrap().cmp_fn)(a, b).reverse()
            });
        }
        let mut rows: Vec<Html> = vec![];
        for i in 0..data.len() {
            let row_data = data_sorted.get(i).unwrap().clone();
            let cells = columns.iter().map(|col_def| {
                let class = match &col_def.class {
                    None => col_def.alignment.as_class(),
                    Some(cls) => format!("{} {cls}", col_def.alignment.as_class()),
                };
                match &col_def.bg_color {
                    Some(bg_color) => {
                        let color = bg_color(&row_data);
                        if color.is_empty() {
                            html! {
                                <td class={class}>
                                    {(col_def.content)(&row_data)}
                                </td>
                            }
                        } else {
                            html! {
                                <td class={class} style={format!("background:{color}", )}>
                                    {(col_def.content)(&row_data)}
                                </td>
                            }
                        }
                    }
                    None => html! {
                    <td class={class}>
                        {(col_def.content)(&row_data)}
                    </td>},
                }
            });
            let cells_html = html! {
                <tr>
                    {for cells}
                </tr>
            };
            rows.push(cells_html);
        }
        html! {
            <table class={classes!("table", "table-hover", "table-sm")} id={table_id.clone()}>
                <thead>
                    <tr>
                        {for headers}
                    </tr>
                </thead>
                <tbody>
                    {for rows}
                </tbody>
            </table>
        }
    }
}
