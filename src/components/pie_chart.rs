use yew::{function_component, html, Html, Properties};

#[derive(PartialEq)]
pub struct HtmlColor(pub String);

#[derive(Properties, PartialEq)]
pub struct PieChartProps {
    pub data: Vec<(HtmlColor, f64, String)>,
}

#[function_component(PieChart)]
pub fn pie_chart(PieChartProps { data }: &PieChartProps) -> Html {
    //let data = vec![("#777", 10.0), ("#DE5B49",20.0)];
    let sum: f64 = data.iter().map(|(_, value, _)| value).sum();
    let mut bg_value = String::default();
    let mut thau = 0.0;
    for (index, (color, value, _label)) in data.iter().enumerate() {
        let angle = value * 360.0 / sum;
        if index > 0 {
            bg_value.push(',');
        }
        bg_value.push_str(&format!(
            "{} {:.1}deg {:.1}deg",
            color.0,
            thau,
            thau + angle
        ));
        thau += angle;
    }
    let labels = data.iter().map(|(color, value, label)| {
        html! {
            <>
            <span class="pie-chart-legend-color" style={format!("background:{};", color.0)}/>
            <span class="pie-chart-legend-name">{label}</span>
            <span class="pie-chart-legend-value">{format!("{:.0}:{:02}", value / 60., value % 60.)}</span>
            <br/>
            </>
        }
    });
    html! {
        <div class="row">
            <div class="col-md-auto pie-chart-circle" style={format!("background-image:conic-gradient({bg_value})")}></div>
            <div class="col pie-chart-legend">
            {for labels}
            </div>
        </div>
    }
}
