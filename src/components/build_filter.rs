//use gloo_utils::document;
//use wasm_bindgen::JsCast;
use yew::{function_component, html, Callback, Html, Properties};

use crate::builds::PlayerBuildData;

#[derive(Properties, PartialEq)]
pub struct BuildFilterProps {
    pub filterer_builds: yew::UseStateHandle<Vec<PlayerBuildData>>,
}

#[function_component(BuildFilter)]
pub fn build_filter(BuildFilterProps { filterer_builds }: &BuildFilterProps) -> Html {
    /*let available_build_names = vec![
        "Condi Holo".to_string(),
        "Power Holo".to_string(),
        "Power Weaver".to_string(),
    ];*/
    let remove_tag = {
        let tags = filterer_builds.clone();
        move |tag: &String| {
            let mut new_tags = vec![];
            for i in 0..tags.len() {
                let t = tags.get(i).unwrap();
                if tag != &t.name {
                    new_tags.push(t.clone())
                }
            }
            tags.set(new_tags);
        }
    };
    //let txt_build_filter = document().get_element_by_id("txtBuildFilter");
    /*let value: String = match txt_build_filter {
        None => "".to_string(),
        Some(x) => x.dyn_into::<web_sys::HtmlInputElement>().unwrap().value(),
    };*/

    /*let autocomplete_suggestions: Vec<String> = available_build_names
    .into_iter()
    .filter(|build_name| build_name.starts_with(value.as_str()))
    .collect();*/
    /*let autocomplete_suggestions_html = autocomplete_suggestions.iter().map(|sug| {
        html! {
            <li>{sug.clone()}</li>
        }
    });*/
    let tags_html = filterer_builds.iter().map(|tag| {
        let tag = tag.clone();
        let remove_tag = remove_tag.clone();
        html! {
            <li class="badge b-form-tag d-inline-flex align-items-baseline mw-100 bg-secondary">
                <span class="b-form-tag-content flex-grow-1 text-truncate">
                {{tag.name.clone()}}
                </span>
                <button class="close b-form-tag-remove" aria-keyshortcuts="Delete" aria-label="Remove tag" onclick={
                    Callback::from( move |_| remove_tag(&tag.name.clone()))
                }>{"×"}</button>
            </li>
        }
    });
    html! {
        <div class="b-form-tags form-control">
            <ul class="b-form-tags-list list-unstyled mb-0 d-flex flex-wrap align-items-center">
                {for tags_html}
                //<li class="b-form-tags-field flex-grow-1">
                //    <div class="d-flex">
                //        <input id="txtBuildFilter" class="b-form-tags-input w-100 flex-grow-1 p-0 m-0 bg-transparent border-0" type="text" placeholder="Add builds ..."/>
                //        <ul>
                //            {for autocomplete_suggestions_html}
                //        </ul>
                //    </div>
                //</li>
            </ul>
        </div>
    }
}
