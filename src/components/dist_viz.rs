use std::cmp::Ordering;

use yew::prelude::*;
use yew::{html, Html};

#[function_component(DistViz)]
pub fn dist_viz(
    DistVizProps {
        data,
        class_name,
        global_max_value,
    }: &DistVizProps,
) -> Html {
    const WIDTH: f64 = 150.0;
    const HEIGHT: f64 = 33.0;

    let mut data_sorted = data.clone();
    data_sorted.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));
    let min_val = data_sorted.first().unwrap();
    let max_val = data_sorted.last().unwrap();
    let len = data.len();
    let p021_val = data_sorted
        .get(((len - 1) as f64 * 0.021 + 0.5) as usize)
        .unwrap();
    let p979_val = data_sorted
        .get(((len - 1) as f64 * 0.979 + 0.5) as usize)
        .unwrap();
    let p16_val = data_sorted
        .get(((len - 1) as f64 * 0.16 + 0.5) as usize)
        .unwrap();
    let p84_val = data_sorted
        .get(((len - 1) as f64 * 0.84 + 0.5) as usize)
        .unwrap();
    let min_x1 = *min_val as f64 * WIDTH / *global_max_value as f64;
    let max_x2 = *max_val as f64 * WIDTH / *global_max_value as f64;
    let x_95 = *p021_val as f64 * WIDTH / *global_max_value as f64;
    let x_68;
    let width_95 = (p979_val - p021_val) as f64 * WIDTH / *global_max_value as f64;
    let mut width_68 = (p84_val - p16_val) as f64 * WIDTH / *global_max_value as f64;
    if width_68 <= 2.0 {
        width_68 = 2.0;
        x_68 = (*p16_val as f64 * WIDTH / *global_max_value as f64) - 1.0;
    } else {
        x_68 = *p16_val as f64 * WIDTH / *global_max_value as f64;
    }
    let mut grid = Vec::new();
    for i in 0..(global_max_value / 10000) {
        let x =
            ((i + 1) as f64 * 10000.0f64 * WIDTH / *global_max_value as f64) as i32 as f64 + 0.5;
        grid.push(
            html! {<line x1={x.to_string()} x2={x.to_string()} y1={0} y2 ={HEIGHT.to_string()} />},
        );
    }
    let y_mid = HEIGHT / 2.0;
    let h95 = HEIGHT - 24.0;
    let h68 = HEIGHT - 18.0;
    html! {
        <div title={format!("Max: {}", max_val)}>
            <svg class={classes!("dist-viz")} width={WIDTH.to_string()} height={32}>
                {for grid}
                <line x1={min_x1.to_string()} x2={max_x2.to_string()} y1={y_mid.to_string()} y2={y_mid.to_string()} style={"stroke-width:1.5"}/>
                <rect class={classes!(format!("percentile-95-{}", class_name))} x={x_95.to_string()} y={12} width={width_95.to_string()} height={h95.to_string()}/>
                <rect class={classes!(format!("percentile-68-{}", class_name))} x={x_68.to_string()} y={9} width={width_68.to_string()} height={h68.to_string()}/>
            </svg>
        </div>
    }
}

#[derive(Properties, PartialEq, Clone)]
pub struct DistVizProps {
    pub data: Vec<i32>,
    pub class_name: String,
    pub global_max_value: i32,
}
