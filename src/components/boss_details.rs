use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::ops::Deref;
use std::sync::Arc;

use chrono::{DateTime, NaiveDate, Utc};
use gloo_console::log;

use gloo_utils::window;
use serde::{Deserialize, Serialize};
use wasm_bindgen::JsCast;
use yew::prelude::*;
use yew::{html, Component, Context, Html};
use yew_bootstrap::component::Spinner;
use yew_bootstrap::util::Color as BTSColor;

use crate::builds::{BuildGuesser, PlayerBuildData};
use crate::components::build_table::BuildTable;
use crate::components::player_icon::PlayerIcon;
use crate::components::table_v2::{TableColAlignment, TableHeaderEntry, TableV2};
use crate::indexed_db_binding::IdbHandle;
use crate::short_log_data::{ShortLogData, ShortLogInfo, ShortLogPlayer};
use elite_insight_json::gw2_data;
use elite_insight_json::gw2_data::{DamageModID, GW2Encounter, GW2SkillID};
use elite_insight_json::log_manager::LMEncounter;

#[derive(Clone, Deserialize, Serialize)]
pub struct BossLogUrlsData {
    encounter_name: String,
    failed_log_urls: Vec<String>,
    successful_log_urls: Vec<String>,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct BossLogsData {
    encounter_name: String,
    failed_logs: Vec<ShortLogInfo>,
    successful_logs: Vec<ShortLogInfo>,
}

#[derive(Clone)]
pub struct BossDetails {
    is_loading_details: bool,
    loaded_logs: BossLogsData,
    displayed_boss: Option<LMEncounter>,
    boss_in_loading: Option<LMEncounter>,
    build_guesser: BuildGuesser,
    show_more_columns: bool,
}

pub enum BossDetailMessage {
    RequestBossDisplayMsg(LMEncounter),
    LoadLogsMsg,
    BossLogsLoadedMsg(BossLogsData),
    ShowMoreColsMessage(bool),
}

#[derive(Properties, PartialEq)]
pub struct LogCountData {
    pub logs_total: u32,
    pub logs_processed: u32,
    pub logs_loaded: u32,
}

#[derive(Properties, PartialEq)]
pub struct BossDetailsProps {
    pub boss: GW2Encounter,
    pub log_counts: UseStateHandle<LogCountData>,
    pub date_filter: UseStateHandle<LogDateFilter>,
    pub mode_filter: UseStateHandle<LogModeFilter>,
    pub build_filter: UseStateHandle<Vec<PlayerBuildData>>,
}

#[derive(Properties, PartialEq)]
pub struct LogDateFilter {
    pub date_after: NaiveDate,
    pub date_before: NaiveDate,
}

#[derive(Properties, PartialEq)]
pub struct LogModeFilter {
    pub allow_cm: bool,
    pub allow_nm: bool,
    pub allow_emboldened: bool,
}

struct Color {
    r: u8,
    g: u8,
    b: u8,
}

impl Color {
    fn to_hex_string(&self) -> String {
        format!("#{:02x}{:02x}{:02x}55", self.r, self.g, self.b)
    }
    fn mix(&self, other: &Color, mixer: f64) -> Color {
        Color {
            r: (self.r as f64 * (1.0 - mixer) + other.r as f64 * mixer) as u8,
            g: (self.g as f64 * (1.0 - mixer) + other.g as f64 * mixer) as u8,
            b: (self.b as f64 * (1.0 - mixer) + other.b as f64 * mixer) as u8,
        }
    }
}

pub fn get_color_from_gradient(
    value: f64,
    red_value: f64,
    yellow_value: f64,
    green_value: f64,
) -> String {
    // NaN check
    if value != value {
        return "".to_string();
    }
    let max_green = Color {
        r: 0,
        g: 255,
        b: 100,
    };
    let mid_green = Color {
        r: 90,
        g: 220,
        b: 0,
    };
    let yellow = Color {
        r: 180,
        g: 180,
        b: 0,
    };
    let red = Color { r: 255, g: 0, b: 0 };
    let mid_green_value = (yellow_value + green_value) * 0.5;
    return if value <= red_value {
        red.to_hex_string()
    } else if value <= yellow_value {
        let mixer = (value - red_value) / (yellow_value - red_value);
        red.mix(&yellow, mixer).to_hex_string()
    } else if value <= mid_green_value {
        let mixer = (value - yellow_value) / (mid_green_value - yellow_value);
        yellow.mix(&mid_green, mixer).to_hex_string()
    } else if value <= green_value {
        let mixer = (value - mid_green_value) / (green_value - mid_green_value);
        mid_green.mix(&max_green, mixer).to_hex_string()
    } else {
        max_green.to_hex_string()
    };
}

#[derive(PartialEq, Clone)]
struct ShortLogInfoProp(ShortLogInfo);

impl Properties for ShortLogInfoProp {
    type Builder = ();

    fn builder() -> Self::Builder {
        todo!()
    }
}

impl Deref for ShortLogInfoProp {
    type Target = ShortLogInfo;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Deserialize, Serialize)]
struct LocalStorageBuilds {
    builds: HashMap<u64, PlayerBuildData>,
    version: u32,
}

pub fn gen_player_build_icons(
    row: &ShortLogInfo,
    build_guesser: &BuildGuesser,
    filter_fn: fn(&ShortLogPlayer) -> bool,
) -> Html {
    let icons = row
        .data
        .players
        .iter()
        .filter(|player| filter_fn(player))
        .map(
            |player| match build_guesser.get_by_hash(player.build_hash) {
                None => {
                    html! {{"?"}}
                }
                Some(build) => {
                    html! {
                        <PlayerIcon player={player.clone()} player_build={build} />
                    }
                }
            },
        );
    html! { for icons }
}

pub fn get_boon_entry(
    row: &ShortLogInfo,
    skill_id: i32,
    cv_r: f64,
    cv_y: f64,
    cv_g: f64,
    percent: bool,
) -> Html {
    let mut sub_uptime_data = row.data.get_sub_uptimes(skill_id);
    sub_uptime_data.sort_by(|(sub1, _), (sub2, _)| sub1.cmp(sub2));
    let mut subs_html = vec![];
    let mut row_index = 1;
    let unit = match percent {
        true => "%",
        false => "",
    };
    for (sub, uptime) in sub_uptime_data {
        let style = format!(
            "background: {}; grid-column: 2; grid-row: {row_index}",
            get_color_from_gradient(uptime, cv_r, cv_y, cv_g)
        );
        subs_html.push(html! {
            <span style={style}> {format!("G{sub}: {uptime:.0}{unit}")} </span>
        });
        row_index += 1;
    }
    let uptime = row.data.get_squad_uptime(skill_id);
    html! {
        <>
            <span class="boon-grid-main" style={format!("background:{}",get_color_from_gradient(uptime, cv_r, cv_y, cv_g))}>
            {format!("{:.1}{unit}", uptime)}
            </span>
            { for subs_html }
        </>
    }
}

impl Component for BossDetails {
    type Message = BossDetailMessage;
    type Properties = BossDetailsProps;

    fn create(_ctx: &Context<Self>) -> Self {
        let build_guesser = BuildGuesser::new();
        BossDetails {
            is_loading_details: false,
            loaded_logs: BossLogsData {
                encounter_name: "".to_string(),
                failed_logs: vec![],
                successful_logs: vec![],
            },
            show_more_columns: false,
            displayed_boss: None,
            boss_in_loading: None,
            build_guesser,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let boss = ctx.props().boss.clone();
        window().document().unwrap().set_title(boss.name.as_str());
        return match msg {
            BossDetailMessage::RequestBossDisplayMsg(boss) => {
                log!("[U-RBD]");
                self.boss_in_loading = Some(boss);
                self.loaded_logs = BossLogsData {
                    encounter_name: "loading ...".to_string(),
                    failed_logs: vec![],
                    successful_logs: vec![],
                };
                ctx.link().send_message(BossDetailMessage::LoadLogsMsg);
                false
            }
            BossDetailMessage::LoadLogsMsg => {
                log!("[U-LML]");
                self.is_loading_details = true;
                ctx.link().send_future(async move {
                    let indexed_db = IdbHandle::open().await;
                    let mut data = BossLogsData {
                        encounter_name: boss.name.clone(),
                        failed_logs: vec![],
                        successful_logs: vec![],
                    };
                    data.successful_logs = indexed_db
                        .get_logs_for_boss::<ShortLogData, ShortLogInfo>(
                            &boss.name_id,
                            |log_id, data| ShortLogInfo {
                                url: format!("https://dps.report/{}", log_id),
                                data,
                            },
                        )
                        .await
                        .unwrap_or(vec![]);
                    BossDetailMessage::BossLogsLoadedMsg { 0: data }
                });
                true
            }
            BossDetailMessage::BossLogsLoadedMsg(logs) => {
                let encounter = gw2_data::get_encounter_by_name(&logs.encounter_name).unwrap();
                log!(
                    "[U-BLL] Now displaying",
                    encounter.name.clone(),
                    "with",
                    logs.successful_logs.len(),
                    "short logs"
                );
                self.displayed_boss = Some(encounter.id);
                self.loaded_logs = logs;
                self.is_loading_details = false;
                true
            }
            BossDetailMessage::ShowMoreColsMessage(show) => {
                self.show_more_columns = show;
                true
            }
        };
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        // todo build_guesser from db
        let build_guesser = Arc::new(BuildGuesser::new());
        let build_guesser_q = build_guesser.clone();
        let build_guesser_a = build_guesser.clone();
        let build_guesser_o = build_guesser.clone();
        let build_guesser_h = build_guesser.clone();
        log!(
            "[VIEW]  data has",
            self.loaded_logs.successful_logs.len(),
            "logs"
        );
        let mut successful_log_sorted: Vec<ShortLogInfo> = self
            .loaded_logs
            .successful_logs
            .clone()
            .into_iter()
            .filter(|log| {
                let date = log.data.date();
                let mode_filter = &ctx.props().mode_filter;
                date <= ctx.props().date_filter.date_before
                    && date >= ctx.props().date_filter.date_after
                    && ((log.data.is_cm && mode_filter.allow_cm)
                        || (!log.data.is_cm && mode_filter.allow_nm))
            })
            .filter(|log| {
                // search for logs that have all the builds we are looking for
                let mut missing_hashs: HashSet<_> = ctx
                    .props()
                    .build_filter
                    .iter()
                    .map(|build| build.hash)
                    .collect();
                for player in log.data.players.iter() {
                    missing_hashs.remove(&player.build_hash);
                    if missing_hashs.len() == 0 {
                        return true;
                    }
                }
                false
            })
            .collect();
        let kill_times_per_healer = HashMap::<i8, Vec<i64>>::new();
        successful_log_sorted.sort_by(|a, b| a.data.start_time.cmp(&b.data.start_time));
        log!(
            "[VIEW]  data filtered and sorted has",
            successful_log_sorted.len(),
            "logs"
        );

        let mut avg_dps_of_bench_count = 0;
        let mut avg_dps_of_bench_sum = 0.0;

        // build stats
        let mut builds = HashMap::<String, PlayerBuildData>::new();
        let mut build_counts = HashMap::<String, i32>::new();
        let mut build_dps_sum = HashMap::<String, i32>::new();
        let mut build_dps_max = HashMap::<String, i32>::new();
        let mut build_cc_sum = HashMap::<String, f64>::new();
        let mut build_dps_data = HashMap::<String, Vec<i32>>::new();
        let mut total_max_dps = 0;
        let mut build_uptime_sum = HashMap::<String, f64>::new();
        for log in &successful_log_sorted {
            for player in &log.data.players {
                let player_build_option = self.build_guesser.get_by_hash(player.build_hash);
                if player_build_option.is_none() {
                    continue;
                }
                let player_build = player_build_option.unwrap();
                if !builds.contains_key(player_build.name.as_str()) {
                    builds.insert(player_build.name.clone(), player_build.clone());
                    build_counts.insert(player_build.name.clone(), 0);
                    build_dps_sum.insert(player_build.name.clone(), 0);
                    build_dps_max.insert(player_build.name.clone(), 0);
                    build_cc_sum.insert(player_build.name.clone(), 0.0);
                    build_dps_data.insert(player_build.name.clone(), vec![]);
                    if player_build.alac || player_build.quick {
                        build_uptime_sum.insert(player_build.name.clone(), 0.0);
                    }
                }

                build_counts.insert(
                    player_build.name.clone(),
                    build_counts[&player_build.name] + 1,
                );
                build_dps_sum.insert(
                    player_build.name.clone(),
                    build_dps_sum[&player_build.name] + player.target_dps,
                );
                if player.target_dps > total_max_dps {
                    total_max_dps = player.target_dps;
                }
                if player.target_dps > build_dps_max[&player_build.name] {
                    build_dps_max.insert(player_build.name.clone(), player.target_dps);
                }
                build_cc_sum.insert(
                    player_build.name.clone(),
                    build_cc_sum[&player_build.name] + player.target_cc,
                );
                build_dps_data
                    .get_mut(player_build.name.as_str())
                    .unwrap()
                    .push(player.target_dps);
                if player_build.alac {
                    build_uptime_sum.insert(
                        player_build.name.clone(),
                        build_uptime_sum[&player_build.name]
                            + log.data.get_group_uptime(
                                GW2SkillID::AlacrityBuff as i32,
                                player.sub_group,
                            ),
                    );
                } else if player_build.quick {
                    build_uptime_sum.insert(
                        player_build.name.clone(),
                        build_uptime_sum[&player_build.name]
                            + log.data.get_group_uptime(
                                GW2SkillID::QuicknessBuff as i32,
                                player.sub_group,
                            ),
                    );
                }
                if let Some(benchmark) = player_build.benchmark {
                    avg_dps_of_bench_count += 1;
                    avg_dps_of_bench_sum += player.target_dps as f64 / benchmark as f64;
                }
            }
        }
        let dps_build_counts: HashMap<String, i32> = build_counts
            .clone()
            .into_iter()
            .filter(|(build_name, _)| {
                let build = builds.get(build_name.deref()).unwrap();
                !build.quick & &!build.alac
            })
            .collect();
        let quickness_build_counts: HashMap<String, i32> = build_counts
            .clone()
            .into_iter()
            .filter(|(build_name, _)| builds.get(build_name.deref()).unwrap().quick)
            .collect();
        let alac_build_counts: HashMap<String, i32> = build_counts
            .clone()
            .into_iter()
            .filter(|(build_name, _)| builds.get(build_name.deref()).unwrap().alac)
            .collect();
        log!(format!("[VIEW]  build count:{}", dps_build_counts.len()));
        let successful_log_sorted_prop: Vec<ShortLogInfoProp> = successful_log_sorted
            .clone()
            .into_iter()
            .map(|sld| ShortLogInfoProp(sld))
            .collect();
        // let build_guesser = self.build_guesser.clone();
        // TODO use single build guesser instance
        let mut successful_logs_colums = vec![
            TableHeaderEntry::sortable_w(
                "Date".to_string(),
                Box::new(|row: &ShortLogInfoProp| {
                    let date: DateTime<Utc> = DateTime::from_timestamp_millis(row.data.start_time)
                        .expect("Failed to convert timestamp");
                    html! {date.format("%d.%m.%Y")}
                }),
                Box::new(|a, b| a.data.start_time.cmp(&b.data.start_time)),
                100,
            ),
            TableHeaderEntry::sortable_w(
                "Duration".to_string(),
                Box::new(|row| {
                    let mins = row.data.duration / (60 * 1000);
                    let secs = (row.data.duration as f64 / (1000.0)) % 60.0;
                    html! { format!("{:02}:{:0>5.2}", mins, secs)}
                }),
                Box::new(|a, b| a.data.duration.cmp(&b.data.duration)),
                80,
            ),
            TableHeaderEntry {
                name: "Alac".to_string(),
                content: Box::new(move |row| {
                    gen_player_build_icons(row, &build_guesser_a, |player| {
                        player
                            .group_buff_generation
                            .get(&(GW2SkillID::AlacrityBuff as i32))
                            .unwrap()
                            + player
                                .off_group_buff_generation
                                .get(&(GW2SkillID::AlacrityBuff as i32))
                                .unwrap()
                            >= 30.
                    })
                }),
                alignment: TableColAlignment::AlignEnd,
                min_width: Some(48),
                ..Default::default()
            },
            TableHeaderEntry {
                name: "A%".to_string(),
                content: Box::new(|row| {
                    get_boon_entry(row, GW2SkillID::AlacrityBuff as i32, 50.0, 77.0, 98.0, true)
                }),
                cmp_fn: Box::new(|a, b| {
                    a.data
                        .get_squad_uptime(GW2SkillID::AlacrityBuff as i32)
                        .total_cmp(&b.data.get_squad_uptime(GW2SkillID::AlacrityBuff as i32))
                }),
                is_sortable: true,
                class: Some("boon-grid-td".to_owned()),
                min_width: Some(95),
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Quick".to_string(),
                content: Box::new(move |row| {
                    gen_player_build_icons(row, &build_guesser_q, |player| {
                        player
                            .group_buff_generation
                            .get(&(GW2SkillID::QuicknessBuff as i32))
                            .unwrap()
                            + player
                                .off_group_buff_generation
                                .get(&(GW2SkillID::QuicknessBuff as i32))
                                .unwrap()
                            >= 30.
                    })
                }),
                alignment: TableColAlignment::AlignEnd,
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Q%".to_string(),
                content: Box::new(|row| {
                    get_boon_entry(
                        row,
                        GW2SkillID::QuicknessBuff as i32,
                        50.0,
                        77.0,
                        98.0,
                        true,
                    )
                }),
                cmp_fn: Box::new(|a, b| {
                    a.data
                        .get_squad_uptime(GW2SkillID::QuicknessBuff as i32)
                        .total_cmp(&b.data.get_squad_uptime(GW2SkillID::QuicknessBuff as i32))
                }),
                is_sortable: true,
                min_width: Some(104),
                class: Some("boon-grid-td".to_owned()),
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Other".to_string(),
                content: Box::new(move |row| {
                    gen_player_build_icons(row, &build_guesser_o, |player| {
                        player
                            .group_buff_generation
                            .get(&(GW2SkillID::QuicknessBuff as i32))
                            .unwrap()
                            + player
                                .off_group_buff_generation
                                .get(&(GW2SkillID::QuicknessBuff as i32))
                                .unwrap()
                            < 30.
                            && player
                                .group_buff_generation
                                .get(&(GW2SkillID::AlacrityBuff as i32))
                                .unwrap()
                                + player
                                    .off_group_buff_generation
                                    .get(&(GW2SkillID::AlacrityBuff as i32))
                                    .unwrap()
                                < 30.
                    })
                }),
                min_width: Some(128),
                cmp_fn: Box::new(|_, _| Ordering::Equal),
                is_sortable: false,
                alignment: TableColAlignment::AlignStart,
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Might".to_string(),
                content: Box::new(|row| {
                    get_boon_entry(row, GW2SkillID::MightBuff as i32, 13.0, 19.0, 24.5, false)
                }),
                cmp_fn: Box::new(|a, b| {
                    a.data
                        .get_squad_uptime(GW2SkillID::MightBuff as i32)
                        .total_cmp(&b.data.get_squad_uptime(GW2SkillID::MightBuff as i32))
                }),
                is_sortable: true,
                class: Some("boon-grid-td".to_owned()),
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Prot".to_string(),
                content: Box::new(|row| {
                    html! { format!("{:.1}%", row.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32)) }
                }),
                cmp_fn: Box::new(|a, b| {
                    a.data
                        .get_squad_uptime(GW2SkillID::ProtectionBuff as i32)
                        .total_cmp(&b.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32))
                }),
                is_sortable: true,
                bg_color: Some(Box::new(|row| {
                    let uptime = row.data.get_squad_uptime(GW2SkillID::ProtectionBuff as i32);
                    get_color_from_gradient(uptime, 0.0, 55.0, 98.0)
                })),
                alignment: TableColAlignment::AlignStart,
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Sc.".to_string(),
                content: Box::new(|row| {
                    let uptime = row.data.get_squad_uptime(DamageModID::Scholar as i32);
                    if uptime > 0.0 {
                        html! { format!("{:.1}%", uptime) }
                    } else {
                        Html::default()
                    }
                }),
                cmp_fn: Box::new(|a, b| {
                    a.data
                        .get_squad_uptime(DamageModID::Scholar as i32)
                        .total_cmp(&b.data.get_squad_uptime(DamageModID::Scholar as i32))
                }),
                is_sortable: true,
                bg_color: Some(Box::new(|row| {
                    let uptime = row.data.get_squad_uptime(DamageModID::Scholar as i32);
                    if uptime == 0.0 {
                        "transparent".to_string()
                    } else {
                        get_color_from_gradient(uptime, 50.0, 77.0, 98.0)
                    }
                })),
                alignment: TableColAlignment::AlignStart,
                ..Default::default()
            },
            TableHeaderEntry {
                name: "#Healer".to_string(),
                content: Box::new(move |row| {
                    let heal_count = row.data.players.iter().fold(0, |state, player| {
                        let build = build_guesser_h.get_by_hash(player.build_hash);
                        match build {
                            None => state,
                            Some(build) => {
                                if build.heal {
                                    1 + state
                                } else {
                                    state
                                }
                            }
                        }
                    });
                    html! {heal_count}
                }),
                alignment: TableColAlignment::AlignStart,
                ..Default::default()
            },
            TableHeaderEntry {
                name: "Log".to_string(),
                content: Box::new(
                    |row| html! { <a href={row.url.clone()} target={"_blank"}>{"Link"}</a> },
                ),
                alignment: TableColAlignment::AlignStart,
                ..Default::default()
            },
        ];
        if self.show_more_columns {
            let players_with_colun = vec![
                ("pv", "PVzweiundvierzig.7381"),
                ("kerchu", "Kerchu.1906"),
                ("ewayne", "Aegas.1069"),
                ("Ent", "NotOverlyCheesy.9427"),
                ("Ines", "CaptainBloodloss.5732"),
                ("RDEF", "RDEF.8513"),
                ("Risaya", "RisayaKinan.9876"),
                ("Stubby", "Bobby.5261"),
                ("oingo", "oingo boingo.7095"),
                ("flare", "flare.8627"), //("flare", ""),
            ];
            for (name, acc) in players_with_colun {
                successful_logs_colums.push(TableHeaderEntry {
                    name: format!("{name} boons"),
                    content: Box::new(|log| {
                        log.data.get_player(acc).map_or(
                            Html::default(),
                            |p| html! { format!("{:.0}%" ,p.combined_uptime_mqa()) },
                        )
                    }),
                    cmp_fn: Box::new(|a, b| -> Ordering {
                        let a_uptime = a
                            .data
                            .get_player(acc)
                            .map(|p| p.combined_uptime_mqa())
                            .unwrap_or(0.0);
                        let b_uptime = b
                            .data
                            .get_player(acc)
                            .map(|p| p.combined_uptime_mqa())
                            .unwrap_or(0.0);
                        a_uptime.partial_cmp(&b_uptime).unwrap_or(Ordering::Equal)
                    }),
                    is_sortable: true,
                    bg_color: Some(Box::new(|row| {
                        let uptime = row.data.get_player(acc).map(|p| p.combined_uptime_mqa());
                        match uptime {
                            Some(uptime) => get_color_from_gradient(uptime, 50.7, 76.7, 98.0),
                            None => "".to_owned(),
                        }
                    })),
                    ..Default::default()
                });
                successful_logs_colums.push(TableHeaderEntry {
                    name: format!("{name} dps"),
                    content: Box::new(|log| {
                        log.data
                            .get_player(acc)
                            .map_or(Html::default(), |p| html! { p.target_dps })
                    }),
                    is_sortable: true,
                    cmp_fn: Box::new(|a, b| -> Ordering {
                        let a_dps = a.data.get_player(acc).map(|p| p.target_dps).unwrap_or(0);
                        let b_dps = b.data.get_player(acc).map(|p| p.target_dps).unwrap_or(0);
                        a_dps.cmp(&b_dps)
                    }),
                    alignment: TableColAlignment::AlignEnd,
                    ..Default::default()
                });
            }
        }
        let on_toggle_more_cols = {
            //let more = mode_filters.clone();
            let link = ctx.link().clone();
            Callback::from(move |ev: Event| {
                let target: Option<web_sys::EventTarget> = ev.target();
                let input = target.and_then(|t| t.dyn_into::<web_sys::HtmlInputElement>().ok());
                if let Some(input) = input {
                    link.send_message(BossDetailMessage::ShowMoreColsMessage(input.checked()));
                }
            })
        };
        let boss_specific = match self.displayed_boss {
            Some(LMEncounter::VG) => "Tank has a purple outline.",
            Some(LMEncounter::Gorseval) => "Tank has a purple outline.",
            Some(LMEncounter::KC) => "Tank has a purple outline.",
            Some(LMEncounter::Deimos) => "Tank has a purple outline. Due to the teleports boon uptimes may be inaccurate.",
            Some(LMEncounter::Xera) => "Only main fight phases are taken into account. Tank has a purple outline.",
            Some(LMEncounter::SH) => "Tanks have a purple outline.",
            Some(LMEncounter::Dhuum) => "Only the main fight and the ritual phase are taken into account. Tank has a purple outline.",
            Some(LMEncounter::Qadim) => "Only the main Qadim phases and the main add phases (Hydra, Destroyer, Wyverns) are taken into account. Players entering the lamp at least twice have a green outline.",
            Some(LMEncounter::Adina) => "Only times when either a Hand or Adina is vulnerable are taken into acoount.",
            Some(LMEncounter::Sabir) => "Platform transition times are not taken into acoount.",
            Some(LMEncounter::QTP) => "Pylon kiters have a green outline. Tank has a purple outline.",
            Some(_) => "-",
            None => "",
        };
        html! {
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <button class="nav-link active" id="builds-tab" data-bs-toggle="tab" data-bs-target="#builds" type="button" role="tab" aria-controls="builds" aria-selected="true">
                        {"Builds"}</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="logs-tab" data-bs-toggle="tab" data-bs-target="#logs" type="button" role="tab" aria-controls="logs" aria-selected="false">
                        {"Logs"}</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="stats-tab" data-bs-toggle="tab" data-bs-target="#stats" type="button" role="tab" aria-controls="stats" aria-selected="false">
                        {"Stats"}</button>
                    </li>
                </ul>
                <div class="tab-content" id="bosses_builds_or_logs">
                    <div class="tab-pane fade show active" id="builds" role="tabpanel" aria-labelledby="builds-tab">
                    if self.is_loading_details {
                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <strong>{"Loading details ..."}</strong>
                            <Spinner style={BTSColor::Primary} class="ms-3"/>
                        </div>
                    } else {
                        <div class="dps-builds">
                            <h3>{"DPS Builds"}</h3>
                            <BuildTable build_counts={dps_build_counts} builds={builds.clone()}
                                build_dps_sum={build_dps_sum.clone()} build_dps_max={build_dps_max.clone()}
                                build_dps_data={build_dps_data.clone()} build_cc_sum={build_cc_sum.clone()}
                                total_max_dps={total_max_dps.clone()} build_uptime_sum={None}/>
                        </div>
                        <div class="alac-builds">
                            <h3>{"Alac Builds"}</h3>
                            <BuildTable build_counts={alac_build_counts} builds={builds.clone()}
                                build_dps_sum={build_dps_sum.clone()} build_dps_max={build_dps_max.clone()}
                                build_dps_data={build_dps_data.clone()} build_cc_sum={build_cc_sum.clone()}
                                total_max_dps={total_max_dps.clone()} build_uptime_sum={Some(build_uptime_sum.clone())}/>
                        </div>
                        <div class="quickness-builds">
                            <h3>{"Quickness Builds"}</h3>
                            <BuildTable build_counts={quickness_build_counts} builds={builds}
                                build_dps_sum={build_dps_sum} build_dps_max={build_dps_max}
                                build_dps_data={build_dps_data} build_cc_sum={build_cc_sum}
                                total_max_dps={total_max_dps} build_uptime_sum={Some(build_uptime_sum.clone())}/>
                        </div>
                    }
                    </div>
                    <div class="successful-attempts tab-pane fade" id="logs" role="tabpanel" aria-labelledby="logs-tab">
                    if self.is_loading_details {
                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <strong>{"Loading details ..."}</strong>
                            <Spinner style={BTSColor::Primary} class="ms-3"/>
                        </div>
                    } else {
                        <div class="input-group mb-1 mt-1">
                            <div class="input-group-text">
                            <input class="form-check-input mt-0" type="checkbox" id="chkMoreColumns" checked={self.show_more_columns} onchange={on_toggle_more_cols}/>
                            </div>
                            <label class="input-group-text bg-body-secondary" for="chkMoreColumns">{"Show player columns"}</label>
                        </div>
                        <TableV2<ShortLogInfoProp> table_id="succesful-attempts-tbl" columns={successful_logs_colums} data={successful_log_sorted_prop} />
                    }
                    </div>
                    <div class="tab-pane fade" id="stats" role="tabpanel" aria-labelledby="stats-tab">
                    if self.is_loading_details {
                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <strong>{"Loading details ..."}</strong>
                            <Spinner style={BTSColor::Primary} class="ms-3"/>
                        </div>
                    } else {

                        {"Number of logs: "} { format!("{}", successful_log_sorted.len())}
                        <br/>
                        {"Avg % of benchmark: "} { format!("{:.1}%",avg_dps_of_bench_sum / avg_dps_of_bench_count as f64 * 100.0)}
                        <br/>
                        {"Boss specific information: "} { boss_specific }
                    }
                    </div>
                    <div class="healer_count_stats">
                        <HealerCountInfo data={kill_times_per_healer}/>
                    </div>
                </div>
            </div>
        }
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            ctx.link().send_message(BossDetailMessage::LoadLogsMsg);
            self.boss_in_loading = Some(ctx.props().boss.id);
        } else if Some(ctx.props().boss.id) != self.boss_in_loading {
            ctx.link()
                .send_message(BossDetailMessage::RequestBossDisplayMsg(
                    ctx.props().boss.id,
                ));
        } else if self.displayed_boss != Some(ctx.props().boss.id)
            && self.displayed_boss == self.boss_in_loading
        {
            //nothing
        }
    }
}

pub fn get_class_from_spec(spec: &String) -> String {
    let spec_str = spec.as_str();
    return match spec_str {
        "Dragonhunter" | "Firebrand" | "Willbender" => "Guardian",
        "Berserker" | "Spellbreaker" | "Bladesworn" => "Warrior",
        "Scrapper" | "Holosmith" | "Mechanist" | "Engineer" => "Engineer",
        "Druid" | "Soulbeast" | "Untamed" => "Ranger",
        "Daredevil" | "Deadeye" | "Specter" | "Thief" => "Thief",
        "Tempest" | "Weaver" | "Catalyst" => "Elementalist",
        "Chronomancer" | "Mirage" | "Virtuoso" => "Mesmer",
        "Reaper" | "Scourge" | "Harbinger" => "Necromancer",
        "Herald" | "Renegade" | "Vindicator" => "Revenant",
        default => default,
    }
    .to_string();
}

#[derive(Properties, PartialEq)]
struct HealerCountInfoProps {
    data: HashMap<i8, Vec<i64>>,
}

fn avg_and_std(data: &[i64]) -> (f64, f64) {
    let len_f64 = data.len() as f64;
    let avg = data.iter().fold(0, |sum, item| sum + item) as f64 / len_f64;
    let variance = data.iter().fold(0.0, |sq_sum, item| {
        sq_sum + (avg - (*item as f64)).powf(2.0)
    }) / len_f64;
    return (avg, variance.sqrt());
}

#[function_component(HealerCountInfo)]
fn healer_count_info(HealerCountInfoProps { data }: &HealerCountInfoProps) -> Html {
    let rows = data.iter().map(|(count, times)| {
        let (avg, std) = avg_and_std(times);
        let mut times_sorted = times.clone();
        times_sorted.sort();
        let median = times_sorted.get(times_sorted.len() / 2).unwrap();
        html! {
            <>
            {format!("{} Healers (#={}): Avg:{:.1}s  Std-deriv:{:.1}s median:{}s", count, times.len() , avg / 1000.0, std / 1000.0, median / 1000)} <br/>
            </>
        }
    });

    html!( <>{ for rows } </>   )
}
