use crate::{builds::PlayerBuildData, short_log_data::ShortLogPlayer};
use yew::{classes, function_component, html, Html, Properties};

#[derive(Properties, PartialEq)]
pub struct PlayerIconProps {
    pub player: ShortLogPlayer,
    pub player_build: PlayerBuildData,
}

#[function_component(PlayerIcon)]
pub fn player_icon(
    PlayerIconProps {
        player_build,
        player,
    }: &PlayerIconProps,
) -> Html {
    let src = format!(
        "/img/{}_tango_icon_20px.png",
        player_build.profession.clone()
    );
    let title;

    if let Some(mark) = player_build.benchmark {
        let percent = 100.0 * player.target_dps as f64 / mark as f64;
        title = format!(
            "{}\n{}\nD/C:{}({percent:.0}%BM)/{}",
            player.name, player_build.name, player.target_dps, player.target_cc
        );
    } else {
        title = format!(
            "{}\n{}\nD/C:{}/{}",
            player.name, player_build.name, player.target_dps, player.target_cc
        );
    }
    let mut classes = match &player.roles {
        None => {
            vec![]
        }
        Some(roles) => roles.iter().map(|role| format!("icon-{}", role)).collect(),
    };
    classes.push("small-icon".to_string());
    if player_build.heal {
        classes.push("icon-heal".to_string());
    }
    html! {
        <img class={classes!(classes)} src={src} alt={player_build.profession.clone()} title={title}/>
    }
}
