use wasm_bindgen::{JsCast, UnwrapThrowExt};
use web_sys::{Event, HtmlInputElement, InputEvent};
use yew::{classes, function_component, html, Callback, Classes, Html, Properties};

#[derive(Clone, PartialEq, Properties)]
pub struct CheckBoxProps {
    #[prop_or_default]
    pub checked: bool,
    #[prop_or_default]
    pub on_change: Callback<(bool, String)>,
    #[prop_or_default]
    pub classes: Classes,
    #[prop_or_default]
    pub id: String,
}

fn get_value_and_id_from_input_event(e: InputEvent) -> (bool, String) {
    let event: Event = e.dyn_into().unwrap_throw();
    let event_target = event.target().unwrap_throw();
    let target: HtmlInputElement = event_target.dyn_into().unwrap_throw();
    (target.checked(), target.id())
}

#[function_component(CheckBox)]
pub fn check_box(props: &CheckBoxProps) -> Html {
    let CheckBoxProps {
        checked,
        on_change,
        classes,
        id,
    } = props.clone();
    let oninput = Callback::from(move |input_event: InputEvent| {
        on_change.emit(get_value_and_id_from_input_event(input_event));
    });
    let class = classes!(classes, "form-check-input");
    html! {
        <input type="checkbox" {checked} {oninput} {class} {id} />
    }
}
