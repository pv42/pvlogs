use std::collections::hash_map::DefaultHasher;
use std::fmt;
use std::hash::{Hash, Hasher};

#[cfg(target_family = "wasm")]
use gloo_console::log;
use serde::{Deserialize, Serialize};

use crate::builds::DamageType::{Condi, Power};
use crate::indexed_db_binding::IdbHandle;
use elite_insight_json::ei_dto::ei_actor::EIActor;
use elite_insight_json::ei_dto::ei_data_types::EIWeaponType;
use elite_insight_json::ei_dto::ei_data_types::EIWeaponType::{
    Axe, Dagger, Greatsword, Hammer, Rifle, Staff, Sword,
};
use elite_insight_json::ei_dto::ei_player::EIPlayer;
use elite_insight_json::gw2_data::GW2SkillID::{AlacrityBuff, QuicknessBuff};

#[derive(Clone, PartialEq, Deserialize, Serialize, Hash, Debug, PartialOrd)]
pub enum DamageType {
    Any,
    Condi,
    Power,
}

impl fmt::Display for DamageType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Clone, PartialEq, Deserialize, Serialize)]
pub struct PlayerBuildData {
    pub name: String,
    pub profession: String,
    pub alac: bool,
    pub quick: bool,
    pub heal: bool,
    pub damage_type: DamageType,
    pub weapons: Vec<EIWeaponType>,
    pub hash: u64,
    pub sc_url: Option<String>,
    pub benchmark: Option<i32>,
}

impl PlayerBuildData {
    pub fn new(
        name: &str,
        profession: &str,
        damage_type: DamageType,
        sc_url: Option<String>,
        benchmark: Option<i32>,
    ) -> PlayerBuildData {
        let mut data = PlayerBuildData {
            name: name.to_string(),
            profession: profession.to_string(),
            alac: false,
            quick: false,
            heal: false,
            damage_type,
            weapons: vec![],
            hash: 0,
            sc_url,
            benchmark,
        };
        data.set_own_hash_str();
        data
    }
    fn new_complex(
        name: &str,
        profession: &str,
        alac: bool,
        quick: bool,
        damage_type: DamageType,
        weapons: Vec<EIWeaponType>,
        sc_url: Option<String>,
        benchmark: Option<i32>,
    ) -> PlayerBuildData {
        let mut data = PlayerBuildData {
            name: name.to_string(),
            profession: profession.to_string(),
            alac,
            quick,
            heal: false,
            damage_type,
            weapons,
            hash: 0,
            sc_url,
            benchmark,
        };
        data.set_own_hash_str();
        data
    }
    fn new_heal(
        name: &str,
        profession: &str,
        alac: bool,
        quick: bool,
        sc_url: Option<String>,
    ) -> PlayerBuildData {
        let mut data = PlayerBuildData {
            name: name.to_string(),
            profession: profession.to_string(),
            alac,
            quick,
            heal: true,
            damage_type: DamageType::Any,
            weapons: vec![],
            hash: 0,
            sc_url,
            benchmark: None,
        };
        data.set_own_hash_str();
        data
    }
    fn set_own_hash_str(&mut self) {
        let mut hasher = DefaultHasher::new();
        self.profession.as_str().hash(&mut hasher);
        self.alac.hash(&mut hasher);
        self.quick.hash(&mut hasher);
        self.heal.hash(&mut hasher);
        self.damage_type.hash(&mut hasher);
        for weapon in &self.weapons {
            weapon.hash(&mut hasher);
        }
        let raw_hash = hasher.finish(); // only use 48bit hash to avoid issues with storage in f64
        self.hash = raw_hash & 0x0000ffffffffffff;
    }
}

#[derive(Clone)]
pub struct BuildGuesser {
    builds: Vec<PlayerBuildData>,
    fallback_build: PlayerBuildData,
}

impl BuildGuesser {
    pub fn new() -> BuildGuesser {
        let builds = vec![
            PlayerBuildData::new_heal(
                "Heal Druid",
                "Druid",
                true,
                false,
                Some("https://snowcrows.com/builds/raids/ranger/heal-druid".to_string()),
            ),
            PlayerBuildData::new_heal("Pre Alac Heal Druid", "Druid", false, false, None),
            PlayerBuildData::new(
                "Power Soulbeast",
                "Soulbeast",
                Power,
                Some("https://snowcrows.com/builds/raids/ranger/power-soulbeast".to_string()),
                Some(42662),
            ),
            PlayerBuildData::new(
                "Condi Soulbeast",
                "Soulbeast",
                Condi,
                Some("https://snowcrows.com/builds/raids/ranger/condition-soulbeast".to_string()),
                Some(41497),
            ),
            PlayerBuildData::new_complex(
                "Staxe Mirage",
                "Mirage",
                true,
                false,
                Condi,
                vec![Axe, Staff],
                Some("https://snowcrows.com/builds/raids/mesmer/condition-mirage".to_string()),
                Some(43048),
            ),
            PlayerBuildData::new_complex(
                "Staff Mirage",
                "Mirage",
                true,
                false,
                Condi,
                vec![Staff, Staff],
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/condition-alacrity-mirage"
                        .to_string(),
                ),
                None,
            ),
            PlayerBuildData::new_complex(
                "Axe Mirage",
                "Mirage",
                false,
                false,
                Condi,
                vec![Axe, Axe],
                None,
                None,
            ),
            PlayerBuildData::new_heal(
                "Heal Quick Chrono",
                "Chronomancer",
                false,
                true,
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/heal-boon-chronomancer".to_string(),
                ),
            ),
            PlayerBuildData::new_heal(
                "Heal Alac Chrono",
                "Chronomancer",
                true,
                false,
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/heal-boon-chronomancer".to_string(),
                ),
            ),
            PlayerBuildData::new_complex(
                "Power Alac Chrono",
                "Chronomancer",
                true,
                false,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/power-boon-chronomancer".to_string(),
                ),
                Some(34271),
            ),
            PlayerBuildData::new_complex(
                "Condi Alac Chrono",
                "Chronomancer",
                true,
                false,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/condition-boon-chronomancer"
                        .to_string(),
                ),
                Some(33455),
            ),
            PlayerBuildData::new_complex(
                "Power Quickness Chrono",
                "Chronomancer",
                false,
                true,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/power-boon-chronomancer".to_string(),
                ),
                Some(34271),
            ),
            PlayerBuildData::new_complex(
                "Condi Quickness Chrono",
                "Chronomancer",
                false,
                true,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/condition-quickness-chronomancer"
                        .to_string(),
                ),
                Some(33455),
            ),
            PlayerBuildData::new(
                "Power Chrono",
                "Chronomancer",
                Power,
                Some("https://snowcrows.com/builds/raids/mesmer/power-chronomancer".to_string()),
                Some(41935),
            ),
            PlayerBuildData::new(
                "Condi Chrono",
                "Chronomancer",
                Condi,
                Some(
                    "https://snowcrows.com/builds/raids/mesmer/condition-chronomancer".to_string(),
                ),
                None,
            ),
            PlayerBuildData::new(
                "Power Virtuoso",
                "Virtuoso",
                Power,
                Some("https://snowcrows.com/builds/raids/mesmer/power-virtuoso".to_string()),
                None,
            ),
            PlayerBuildData::new(
                "Condi Virtuoso",
                "Virtuoso",
                Condi,
                Some("https://snowcrows.com/builds/raids/mesmer/condition-virtuoso".to_string()),
                Some(42547),
            ),
            PlayerBuildData::new_heal("Pre Alac Heal Scourge", "Scourge", false, false, None),
            PlayerBuildData::new_heal(
                "Heal Scourge",
                "Scourge",
                true,
                false,
                Some(
                    "https://snowcrows.com/builds/raids/necromancer/heal-alacrity-scourge"
                        .to_string(),
                ),
            ),
            PlayerBuildData::new(
                "Condi Scourge",
                "Scourge",
                Condi,
                Some(
                    "https://snowcrows.com/builds/raids/necromancer/condition-scourge".to_string(),
                ),
                Some(40060),
            ),
            PlayerBuildData::new(
                "Power Reaper",
                "Reaper",
                Power,
                Some("https://snowcrows.com/builds/raids/necromancer/power-reaper".to_string()),
                Some(41536),
            ),
            PlayerBuildData::new_complex(
                "Quickbringer",
                "Harbinger",
                false,
                true,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/necromancer/condition-quickness-harbinger"
                        .to_string(),
                ),
                Some(35601),
            ),
            PlayerBuildData::new(
                "Condi Harbinger",
                "Harbinger",
                Condi,
                Some(
                    "https://snowcrows.com/builds/raids/necromancer/condition-harbinger"
                        .to_string(),
                ),
                Some(43048),
            ),
            PlayerBuildData::new_heal(
                "Heal Scrapper",
                "Scrapper",
                false,
                true,
                Some(
                    "https://snowcrows.com/builds/raids/engineer/heal-quickness-scrapper"
                        .to_string(),
                ),
            ),
            PlayerBuildData::new_complex(
                "Qrapper",
                "Scrapper",
                false,
                true,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/engineer/power-quickness-scrapper"
                        .to_string(),
                ),
                Some(32146),
            ),
            PlayerBuildData::new_complex(
                "DPS Scrapper",
                "Scrapper",
                false,
                false,
                Power,
                vec![Hammer],
                Some("https://snowcrows.com/builds/raids/engineer/power-scrapper".to_string()),
                Some(43080),
            ),
            PlayerBuildData::new(
                "Condi Holo",
                "Holosmith",
                Condi,
                Some("https://snowcrows.com/builds/raids/engineer/condition-holosmith".to_string()),
                None,
            ),
            PlayerBuildData::new_complex(
                "Sword Holo",
                "Holosmith",
                false,
                false,
                Power,
                vec![Sword],
                None,
                None,
            ),
            PlayerBuildData::new_complex(
                "Rifle Holo",
                "Holosmith",
                false,
                false,
                Power,
                vec![Rifle],
                None,
                None,
            ),
            PlayerBuildData::new_complex(
                "Hammer Holo",
                "Holosmith",
                false,
                false,
                Power,
                vec![Hammer],
                Some("https://snowcrows.com/builds/raids/engineer/power-holosmith".to_string()),
                Some(40793),
            ),
            PlayerBuildData::new_heal(
                "Heal Mechanist",
                "Mechanist",
                true,
                false,
                Some(
                    "https://snowcrows.com/builds/raids/engineer/heal-alacrity-mechanist"
                        .to_string(),
                ),
            ),
            PlayerBuildData::new_complex(
                "Alac Mechanist",
                "Mechanist",
                true,
                false,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/engineer/power-alacrity-mechanist"
                        .to_string(),
                ),
                None,
            ),
            PlayerBuildData::new_complex(
                "Condi Alac Mechanist",
                "Mechanist",
                true,
                false,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/engineer/condition-alacrity-mechanist"
                        .to_string(),
                ),
                Some(33067),
            ),
            PlayerBuildData::new(
                "Power Mechanist",
                "Mechanist",
                Power,
                Some("https://snowcrows.com/builds/raids/engineer/power-mechanist".to_string()),
                None,
            ),
            PlayerBuildData::new(
                "Condi Mechanist",
                "Mechanist",
                Condi,
                Some("https://snowcrows.com/builds/raids/engineer/condition-mechanist".to_string()),
                None,
            ),
            PlayerBuildData::new(
                "Dragonhunter",
                "Dragonhunter",
                Power,
                Some("https://snowcrows.com/builds/raids/guardian/power-dragonhunter".to_string()),
                Some(42404),
            ),
            PlayerBuildData::new_heal(
                "Healbender",
                "Willbender",
                true,
                false,
                Some(
                    "https://snowcrows.com/builds/raids/guardian/heal-alacrity-willbender"
                        .to_string(),
                ),
            ),
            PlayerBuildData::new_complex(
                "Power Alac Willbender",
                "Willbender",
                true,
                false,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/guardian/power-alacrity-willbender"
                        .to_string(),
                ),
                Some(34860),
            ),
            PlayerBuildData::new(
                "Power Willbender",
                "Willbender",
                Power,
                Some("https://snowcrows.com/builds/raids/guardian/power-willbender".to_string()),
                None,
            ),
            PlayerBuildData::new(
                "Condi Willbender",
                "Willbender",
                Condi,
                Some(
                    "https://snowcrows.com/builds/raids/guardian/condition-willbender".to_string(),
                ),
                None,
            ),
            PlayerBuildData::new_heal(
                "Heal Firebrand",
                "Firebrand",
                false,
                true,
                Some(
                    "https://snowcrows.com/builds/raids/guardian/heal-quickness-firebrand"
                        .to_string(),
                ),
            ),
            PlayerBuildData::new_complex(
                "Quickness Firebrand",
                "Firebrand",
                false,
                true,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/guardian/condition-quickness-firebrand"
                        .to_string(),
                ),
                None,
            ),
            PlayerBuildData::new(
                "Condi Firebrand",
                "Firebrand",
                Condi,
                Some("https://snowcrows.com/builds/raids/guardian/condition-firebrand".to_string()),
                Some(39538),
            ),
            PlayerBuildData::new_complex(
                "Power Quickness Berserker",
                "Berserker",
                false,
                true,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/warrior/power-quickness-berserker"
                        .to_string(),
                ),
                Some(37654),
            ),
            PlayerBuildData::new(
                "Power Berserker",
                "Berserker",
                Power,
                Some("https://snowcrows.com/builds/raids/warrior/power-berserker".to_string()),
                Some(42338),
            ),
            PlayerBuildData::new(
                "Condi Berserker",
                "Berserker",
                Condi,
                Some("https://snowcrows.com/builds/raids/warrior/condition-berserker".to_string()),
                Some(41285),
            ),
            PlayerBuildData::new_complex(
                "Hammerbreaker",
                "Spellbreaker",
                false,
                false,
                Power,
                vec![Hammer],
                Some("https://snowcrows.com/builds/raids/warrior/power-spellbreaker".to_string()),
                Some(40468),
            ),
            PlayerBuildData::new_complex(
                "Greatswordbreaker",
                "Spellbreaker",
                false,
                false,
                Power,
                vec![Greatsword],
                None,
                None,
            ),
            PlayerBuildData::new(
                "Power Bladesworn",
                "Bladesworn",
                Power,
                Some("https://snowcrows.com/builds/raids/warrior/power-bladesworn".to_string()),
                Some(42201),
            ),
            PlayerBuildData::new_heal(
                "Heal Renegade",
                "Renegade",
                true,
                false,
                Some("https://snowcrows.com/builds/raids/revenant/heal-renegade".to_string()),
            ),
            PlayerBuildData::new_complex(
                "Power Alacrigade",
                "Renegade",
                true,
                false,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/revenant/power-alacrity-renegade"
                        .to_string(),
                ),
                None,
            ),
            PlayerBuildData::new_complex(
                "Condi Alacrigade",
                "Renegade",
                true,
                false,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/revenant/condition-alacrity-renegade"
                        .to_string(),
                ),
                Some(35493),
            ),
            PlayerBuildData::new(
                "Condi Renegade",
                "Renegade",
                Condi,
                Some("https://snowcrows.com/builds/raids/revenant/condition-renegade".to_string()),
                Some(41670),
            ),
            PlayerBuildData::new_complex(
                "Quickherald",
                "Herald",
                false,
                true,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/revenant/power-quickness-herald"
                        .to_string(),
                ),
                Some(32618),
            ),
            PlayerBuildData::new_complex(
                "Condi Quickherald",
                "Herald",
                false,
                true,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/revenant/condition-quickness-herald"
                        .to_string(),
                ),
                Some(34310),
            ),
            PlayerBuildData::new(
                "Power Herald",
                "Herald",
                Power,
                Some("https://snowcrows.com/builds/raids/revenant/power-herald".to_string()),
                None,
            ),
            PlayerBuildData::new(
                "Power Vindicator",
                "Vindicator",
                Power,
                Some("https://snowcrows.com/builds/raids/revenant/power-vindicator".to_string()),
                None,
            ),
            PlayerBuildData::new_heal(
                "Heal Tempest",
                "Tempest",
                true,
                false,
                Some(
                    "https://snowcrows.com/builds/raids/elementalist/heal-alacrity-tempest"
                        .to_string(),
                ),
            ),
            PlayerBuildData::new_complex(
                "Condi Alac Tempest",
                "Tempest",
                true,
                false,
                DamageType::Any,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/elementalist/condition-alacrity-tempest"
                        .to_string(),
                ),
                Some(33969),
            ), // damage is 50/50
            PlayerBuildData::new(
                "Power Weaver",
                "Weaver",
                Power,
                Some("https://snowcrows.com/builds/raids/elementalist/power-weaver".to_string()),
                Some(42746),
            ),
            PlayerBuildData::new(
                "Condi Weaver",
                "Weaver",
                Condi,
                Some(
                    "https://snowcrows.com/builds/raids/elementalist/condition-weaver".to_string(),
                ),
                Some(42382),
            ),
            PlayerBuildData::new_complex(
                "Quick Catalyst",
                "Catalyst",
                false,
                true,
                Power,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/elementalist/power-quickness-catalyst"
                        .to_string(),
                ),
                None,
            ),
            PlayerBuildData::new(
                "Power Catalyst",
                "Catalyst",
                Power,
                Some("https://snowcrows.com/builds/raids/elementalist/power-catalyst".to_string()),
                Some(43461),
            ),
            PlayerBuildData::new_complex(
                "Boon Thief",
                "Thief",
                false,
                true,
                Power,
                vec![],
                None,
                None,
            ),
            PlayerBuildData::new_complex(
                "Boon DD",
                "Daredevil",
                false,
                true,
                Power,
                vec![Staff],
                None,
                None,
            ),
            PlayerBuildData::new_complex(
                "Condi Boon DD",
                "Daredevil",
                false,
                true,
                Condi,
                vec![],
                None,
                None,
            ),
            PlayerBuildData::new_complex(
                "Power Staff DD",
                "Daredevil",
                false,
                false,
                Power,
                vec![Staff],
                Some("https://snowcrows.com/builds/raids/thief/power-daredevil".to_string()),
                Some(40002),
            ),
            PlayerBuildData::new_complex(
                "Condi Staff DD",
                "Daredevil",
                false,
                false,
                Condi,
                vec![Staff],
                None,
                None,
            ),
            PlayerBuildData::new_complex(
                "Condi Dagger DD",
                "Daredevil",
                false,
                false,
                Condi,
                vec![Dagger, Dagger],
                Some("https://snowcrows.com/builds/raids/thief/condition-daredevil".to_string()),
                Some(41860),
            ),
            PlayerBuildData::new(
                "Power Deadeye",
                "Deadeye",
                Power,
                Some("https://snowcrows.com/builds/raids/thief/power-deadeye".to_string()),
                Some(42906),
            ),
            PlayerBuildData::new(
                "Condi Deadeye",
                "Deadeye",
                Condi,
                Some("https://snowcrows.com/builds/raids/thief/condition-deadeye".to_string()),
                None,
            ),
            PlayerBuildData::new_complex(
                "Alac Specter",
                "Specter",
                true,
                false,
                Condi,
                vec![],
                Some(
                    "https://snowcrows.com/builds/raids/thief/condition-alacrity-spectre"
                        .to_string(),
                ),
                Some(33918),
            ),
            PlayerBuildData::new_complex(
                "Condi Specter",
                "Specter",
                false,
                false,
                Condi,
                vec![],
                Some("https://snowcrows.com/builds/raids/thief/condition-spectre".to_string()),
                Some(41290),
            ),
        ];
        BuildGuesser {
            builds,
            fallback_build: PlayerBuildData {
                name: "Other build".to_string(),
                alac: false,
                quick: false,
                profession: "".to_string(),
                heal: false,
                damage_type: DamageType::Any,
                weapons: vec![],
                hash: 0,
                sc_url: None,
                benchmark: None,
            },
        }
    }

    pub async fn from_db() -> BuildGuesser {
        let db = IdbHandle::open().await;
        let mut builds: Vec<PlayerBuildData> = db
            .get_all_builds()
            .await
            .expect("Failed to get builds from db");
        let fallback_index = builds
            .iter()
            .position(|build| build.hash == 0)
            .expect("There should be fallback build with hash 0 in the database.");
        let fallback_build = builds.remove(fallback_index);
        BuildGuesser {
            builds,
            fallback_build,
        }
    }

    pub fn guess_build(&self, ei_player: &EIPlayer) -> PlayerBuildData {
        let is_quick = ei_player.get_squad_buff_generation_total(QuicknessBuff as i64) > 15.0;
        let is_alac = ei_player.get_squad_buff_generation_total(AlacrityBuff as i64) > 15.0;
        let is_heal = ei_player.healing_rank() >= 5;
        let cdmg = ei_player
            .get_all_targets_damage(elite_insight_json::ei_dto::helper_types::DamageType::Condi);
        let pdmg = ei_player
            .get_all_targets_damage(elite_insight_json::ei_dto::helper_types::DamageType::Power);
        let damage_type = if cdmg > pdmg { Condi } else { Power };
        let matches: Vec<&PlayerBuildData> = self
            .builds
            .iter()
            .filter(|build| {
                true && (!build.quick || is_quick)
                    && (!build.alac || is_alac)
                    && (!build.heal || is_heal)
                    && (build.damage_type == damage_type || build.damage_type == DamageType::Any)
                    && format!("{:?}", ei_player.profession) == build.profession
            })
            .filter(|build| {
                let mut index = 0;
                for weapon in &build.weapons {
                    loop {
                        if &(ei_player.get_all_weapons()[index]) == weapon {
                            break;
                        }
                        index += 1;
                        if index == 8 {
                            return false;
                        }
                    }
                }
                true
            })
            .collect();
        let count = matches.len();
        return if count == 0 {
            self.fallback_build.clone()
        } else {
            if count > 1 {
                if cfg!(target_family = "wasm") {
                    //log!(format!("Found multiple build matches for {}", ei_player.name.clone()));
                }
                /*for build in &matches {
                    //log!(build.name.clone());
                }*/
            }
            matches[0].clone()
        };
    }

    pub fn get_builds(&self) -> Vec<PlayerBuildData> {
        let mut builds = self.builds.clone();
        builds.push(self.fallback_build.clone());
        return builds;
    }

    pub fn get_by_hash(&self, build_hash: u64) -> Option<PlayerBuildData> {
        for build in &self.builds {
            if build.hash == build_hash {
                return Some(build.clone());
            }
        }
        if self.fallback_build.hash == build_hash {
            return Some(self.fallback_build.clone());
        }
        None
    }
}
