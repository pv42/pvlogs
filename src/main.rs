mod builds;
mod components;
mod indexed_db_binding;
mod short_log_data;
mod views;
pub mod yew_main;

fn main() {
    yew_main::yew_main();
}
