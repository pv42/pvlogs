use std::{cmp::min, collections::HashMap, vec};

use chrono::{DateTime, Datelike, Duration, NaiveDate, Utc};
use elite_insight_json::{
    ei_dto::{
        ei_actor::EIActor,
        ei_data_types::{EIPhase, EliteInsightData},
        ei_player::EIPlayer,
        helper_types::PhaseIndex,
    },
    gw2_data::{self, DamageModID, GW2SkillID},
};
use gloo_console::log;
use serde::{Deserialize, Serialize};

use crate::builds::BuildGuesser;

#[derive(Clone, PartialEq, Deserialize, Serialize)]
pub struct OldShortLogPlayerV13 {
    pub target_cc: f64,
    pub target_dps: i32,
    pub build_hash: u64,
    pub buff_uptime: HashMap<i32, f64>,
    pub group_buff_generation: HashMap<i32, f64>,
    pub off_group_buff_generation: HashMap<i32, f64>,
    pub sub_group: i32,
    pub name: String,
    pub roles: Option<Vec<String>>,
}

#[derive(Clone, PartialEq, Deserialize, Serialize)]
pub struct ShortLogPlayer {
    pub target_cc: f64,
    pub target_dps: i32,
    pub build_hash: u64,
    pub buff_uptime: HashMap<i32, f64>,
    pub group_buff_generation: HashMap<i32, f64>,
    pub off_group_buff_generation: HashMap<i32, f64>,
    pub sub_group: i32,
    pub name: String,
    pub roles: Option<Vec<String>>,
    pub active_time: u64,
}

impl ShortLogPlayer {
    pub fn combined_uptime_mqa(&self) -> f64 {
        (self
            .buff_uptime
            .get(&(gw2_data::GW2SkillID::MightBuff as i32))
            .unwrap_or(&0.0)
            * 4.0
            + self
                .buff_uptime
                .get(&(gw2_data::GW2SkillID::QuicknessBuff as i32))
                .unwrap_or(&0.0)
            + self
                .buff_uptime
                .get(&(gw2_data::GW2SkillID::AlacrityBuff as i32))
                .unwrap_or(&0.0))
            / 3.0
    }
}

#[derive(Clone, Deserialize, Serialize, PartialEq)]
pub struct OldShortLogDataV13 {
    pub duration: i64,
    pub is_cm: bool,
    pub start_time: i64,
    pub boss_id: String,
    pub players: Vec<OldShortLogPlayerV13>,
}

impl From<OldShortLogDataV13> for OldShortLogDataV16 {
    fn from(value: OldShortLogDataV13) -> Self {
        let date = DateTime::from_timestamp_millis(value.start_time)
            .expect("Failed to convert timestamp")
            .date_naive();
        let iso_week = date.iso_week();
        OldShortLogDataV16 {
            duration: value.duration,
            is_cm: value.is_cm,
            start_time: value.start_time,
            week: iso_week.week() + iso_week.year() as u32 * 100,
            boss_id: value.boss_id,
            players: value.players,
        }
    }
}

#[derive(Clone, Deserialize, Serialize, PartialEq)]
pub struct OldShortLogDataV16 {
    pub duration: i64,
    pub is_cm: bool,
    pub start_time: i64,
    pub week: u32,
    pub boss_id: String,
    pub players: Vec<OldShortLogPlayerV13>,
}

#[derive(Clone, Deserialize, Serialize, PartialEq)]
pub struct ShortLogData {
    pub duration: i64,
    pub is_cm: bool,
    pub start_time: i64,
    pub week: u32,
    pub boss_id: String,
    pub players: Vec<ShortLogPlayer>,
}

impl ShortLogData {
    pub fn date(&self) -> NaiveDate {
        DateTime::from_timestamp_millis(self.start_time)
            .expect("Failed to convert timestamp")
            .date_naive()
    }

    pub fn get_group_uptime(&self, skill_id: i32, sub_group: i32) -> f64 {
        let mut sum = 0.0;
        let mut total_active_time = 0;
        for player in &self.players {
            if player.sub_group == sub_group {
                total_active_time += player.active_time;
                sum +=
                    player.buff_uptime.get(&skill_id).unwrap_or(&0.0) * player.active_time as f64;
            }
        }
        sum / total_active_time as f64
    }
    pub fn get_sub_uptimes(&self, skill_id: i32) -> Vec<(i32, f64)> {
        let mut data = HashMap::<i32, (i32, f64)>::new();
        for player in &self.players {
            let uptime = player.buff_uptime.get(&skill_id).unwrap_or(&0.0);
            data.entry(player.sub_group)
                .and_modify(|(count, sum)| {
                    *count += 1;
                    *sum += uptime
                })
                .or_insert((1, *uptime));
        }
        data.into_iter()
            .map(|(sub, (count, sum))| (sub, sum / count as f64))
            .collect::<Vec<_>>()
    }
    pub fn get_squad_uptime(&self, skill_id: i32) -> f64 {
        let mut sum = 0.0;
        let mut count = 0;
        for player in &self.players {
            // todo if player.build.name != "NPC" {
            count += 1;
            sum += player.buff_uptime.get(&skill_id).unwrap();
            //}
        }
        sum / count as f64
    }
    pub fn get_player(&self, account_name: &str) -> Option<&ShortLogPlayer> {
        let player_names = match account_name {
            "PVzweiundvierzig.7381" => vec![
                "Carina Caslin",
                "Clyhx",
                "Clyx Caslin",
                "Flame Jet Farming",
                "Lihra Caslin",
                "Lilith Caslin",
                "Maya Caslin",
                "Rayna Caslin",
                "The Nörn",
                "The Silvarii",
                "Lihra Caslin",
                "Ninlil Caslin",
                "Amara Caslin",
            ],
            "Kerchu.1906" => vec![
                "Tsunamika",
                "Richard Durex",
                "Debodda",
                "Part Time Healer",
                "Buucheg",
                "Sticky Claws",
                "Keepitreel",
                "Nekomplèkt",
                "Grand Reverend",
                "Schrottattilerie",
                "Nrrw",
                "Tsunamike",
                "Rebound Was On Cd",
                "Jintoniq",
                "Goldless Kaiji",
            ],
            "Aegas.1069" => vec![
                "Uriel Evaniscus",
                "Sariel Evaniscus",
                "Rei Evaniscus",
                "Amsiael Evaniscus",
                "Abdiel Evaniscus",
                "Suriel Evaniscus",
                "Iofiel Evaniscus",
                "Historia Evaniscus",
                "Artillery Barage",
                "Maria Evaniscus",
                "Estariel Evaniscus",
                // MoreEwayne.1732
                "Rarely Distorts",
                "Loot At That Aegis",
                "Boons With Benefits",
                "Alacrity Dealer",
            ],
            "CaptainBloodloss.5732" => vec![
                "Binna Brakardottir",
                "Doe Snowforge",
                "Anura Bloodveil",
                "Shirin Weir",
                "Big Women Big Fun",
                "Hexa Ruestmane",
                "Dandylion Mindwreck",
                "Kalrova Steelswipe",
                "Era Gearcrown",
            ],
            "NotOverlyCheesy.9427" => vec![
                "Reythina",
                "Mistar Fist",
                "Techo Feline",
                "Fernais",
                "Io Mimoki",
                "Mentis Dividi",
                "Kaikukaarna",
                "Enthum",
            ],
            "RDEF.8513" => vec![
                "Shalis Ironhealer",
                "Spirit Of Podaltur",
                "Arabidopsis Thalia",
            ],
            "RisayaKinan.9876" => vec![
                "Risaya Kinan",
                "Ilyana Kinan",
                "Isandri Kinan",
                "Sarya Kinan",
                "Kiyra Kinan",
                "Ilira Kinan",
                "ELena Kinan",
                "Dari Kinan",
                "Sora Kinan",
            ],
            "Bobby.5261" => vec![
                "Stübby I",
                "I Stubbie I",
                "Stubby Ill",
                "Stúbby",
                "Bomboozled",
                "Stubbíe",
                "Stübby",
                "Stubbié",
            ],
            "oingo boingo.7095" => vec![
                "Oingo Or Boingo",
                "Oingo And Boingos",
                "Oingo And Boingo",
                "Oingonant",
                "Oîngoboîngo",
                "Oingomancer",
                // boingo oingo from here on
                "Boingo Oingo",
                "Boingos And Oingos",
                "Boigonant",
                "Boingomancer",
                "Boingoingo",
            ],
            "flare.8627" => vec![
                "Shlendelzare",
                "Ah Yes One Button",
                "Katheleen Stormborn",
                "Asure Holo Dancer",
                // from flare.3584
                "Shlendelzáre",
                // from flare.6384
                "What Is Alac Anyway",
            ],
            _ => todo!("unknown acc: {}", account_name),
        };
        self.players
            .iter()
            .find(|p| player_names.contains(&p.name.as_str()))
    }
    pub fn get_end_time(&self) -> DateTime<Utc> {
        let msecs: i64 = self.start_time + self.duration;
        DateTime::from_timestamp_millis(msecs).expect("Failed to convert timestamp")
    }
    pub fn get_start_time(&self) -> DateTime<Utc> {
        DateTime::from_timestamp_millis(self.start_time).expect("Failed to convert timestamp")
    }
    pub fn get_duration(&self) -> Duration {
        Duration::milliseconds(self.duration)
    }
}

#[derive(Clone, Deserialize, Serialize)]
pub struct ShortLogInfo {
    pub url: String,
    pub data: ShortLogData,
}

impl PartialEq for ShortLogInfo {
    fn eq(&self, other: &Self) -> bool {
        self.url == other.url
    }
}

fn get_uptime_by_phases(
    phases: &[EIPhase],
    dps_phase_names: Vec<&str>,
    buff_id: i64,
    player: &EIPlayer,
) -> f64 {
    let phase_indices_and_len: Vec<_> = phases
        .iter()
        .enumerate()
        .filter(|(_, phase)| dps_phase_names.contains(&phase.name.as_str()))
        .map(|(index, _)| {
            (
                index,
                player
                    .active_time(PhaseIndex(index))
                    .unwrap_or(Duration::zero()),
            )
        })
        .collect();
    let sum = phase_indices_and_len
        .iter()
        .fold(Duration::zero(), |sum, (_, len)| sum + *len)
        .num_milliseconds();
    let mut uptime = 0.0;
    for (index, len) in phase_indices_and_len {
        uptime += player.get_buff_uptime_phase(buff_id, PhaseIndex(index))
            * len.num_milliseconds() as f64
            / sum as f64;
    }
    if uptime.is_nan() {
        0.0
    } else {
        uptime
    }
}

fn get_uptime_dps_phases(ei_data: &EliteInsightData, player: &EIPlayer, buff_id: i64) -> f64 {
    match ei_data.fight_name() {
        "Dhuum" | "Dhuum CM" => {
            let dps_phase_names = vec!["Dhuum Fight", "Ritual"];
            get_uptime_by_phases(&ei_data.phases, dps_phase_names, buff_id, player)
        }
        "Qadim" | "Qadim CM" => {
            let dps_phase_names = vec![
                "Hydra",
                "Qadim P1",
                "Apocalypse",
                "Qadim P2",
                "Wyvern",
                "Qadim P3",
            ];
            get_uptime_by_phases(&ei_data.phases, dps_phase_names, buff_id, player)
        }
        "Cardinal Adina" | "Cardinal Adina CM" => {
            let dps_phase_names = vec![
                "Phase 1", "Split 1", "Phase 2", "Split 2", "Phase 3", "Split 3", "Phase 4",
            ];
            get_uptime_by_phases(&ei_data.phases, dps_phase_names, buff_id, player)
        }
        "Cardinal Sabir" | "Cardinal Sabir CM" => {
            let dps_phase_names = vec!["Phase 1", "Phase 2", "Phase 3"];
            get_uptime_by_phases(&ei_data.phases, dps_phase_names, buff_id, player)
        }
        "Xera" => {
            let dps_phase_names = vec!["Phase 1", "Phase 2"];
            get_uptime_by_phases(&ei_data.phases, dps_phase_names, buff_id, player)
        }
        _ => player.get_buff_uptime_total(buff_id),
    }
}

pub fn generate_short_log_data(
    ei_data: &EliteInsightData,
    boss_id: &str,
    build_guesser: &BuildGuesser,
) -> ShortLogData {
    ShortLogData {
        duration: ei_data.duration().num_milliseconds(),
        is_cm: ei_data.is_cm(),
        start_time: ei_data.time_start().timestamp() * 1000,
        boss_id: boss_id.to_string(),
        players: generate_short_players(ei_data, build_guesser),
        week: ei_data.time_start().iso_week().week()
            + ei_data.time_start().iso_week().year() as u32 * 100,
    }
}

fn generate_short_players(
    ei_data: &EliteInsightData,
    build_guesser: &BuildGuesser,
) -> Vec<ShortLogPlayer> {
    let mut players = vec![];
    let mut processed_accounts = HashMap::new();
    let mut tank_players = vec![];
    let mut special_players = vec![];
    match ei_data.fight_name() {
        "Soulless Horror CM" | "Soulless Horror" | "ag535 CM" => {
            tank_players = get_top_players_by_mechanic(ei_data, "Fixate", 2, None, 0);
        }
        "Qadim the Peerless" | "ag121" => {
            tank_players =
                get_top_players_by_mechanic(ei_data, "Fixated", 1, Some(ei_data.duration() / 2), 0);
            special_players = get_top_players_by_mechanic(ei_data, "Orb caught", 3, None, 0);
        }
        "Qadim" | "Qadim CM" => {
            special_players = get_top_players_by_mechanic(ei_data, "Lamp", 2, None, 2);
        }
        "Vale Guardian"
        | "Gorseval the Multifarious"
        | "Keep Construct"
        | "Keep Construct CM"
        | "Xera"
        | "Deimos"
        | "Deimos CM"
        | "Dhuum"
        | "Dhuum CM"
        | "Cardinal Adina"
        | "Cardinal Adina CM" => {
            tank_players = get_toughness_tanks(ei_data, 1);
        }
        "Twin Largos" | "Twin Largos CM" => {
            tank_players = get_toughness_tanks(ei_data, 2);
        }
        _ => {}
    }
    for player in ei_data.players.iter() {
        if player
            .active_time(PhaseIndex::full_fight())
            .map(|v| v.is_zero())
            .unwrap_or(false)
        {
            log!(
                "Skipping player with active time 0",
                &player.account().to_owned()
            );
            continue;
        }
        if processed_accounts.contains_key(&player.account()) {
            log!(
                "Found multiple players for account",
                &player.account().to_owned()
            );
            continue;
        }
        processed_accounts.insert(player.account(), players.len());
        players.push(generate_short_player_data(
            ei_data,
            player,
            &tank_players,
            &special_players,
            build_guesser,
        ));
    }
    players
}

fn generate_short_player_data(
    ei_data: &EliteInsightData,
    player: &EIPlayer,
    tank_players: &[EIPlayer],
    special_players: &[EIPlayer],
    build_guesser: &BuildGuesser,
) -> ShortLogPlayer {
    let quickness_uptime = get_uptime_dps_phases(ei_data, player, GW2SkillID::QuicknessBuff as i64);
    let alac_uptime = get_uptime_dps_phases(ei_data, player, GW2SkillID::AlacrityBuff as i64);
    let quickness_group = player.get_group_buff_generation_total(GW2SkillID::QuicknessBuff as i64);
    let quickness_off_group =
        player.get_off_group_buff_generation_total(GW2SkillID::QuicknessBuff as i64);
    let alac_group = player.get_group_buff_generation_total(GW2SkillID::AlacrityBuff as i64);
    let alac_off_group =
        player.get_off_group_buff_generation_total(GW2SkillID::AlacrityBuff as i64);
    let prot_uptime = player.get_buff_uptime_total(GW2SkillID::ProtectionBuff as i64);
    let might_uptime = get_uptime_dps_phases(ei_data, player, GW2SkillID::MightBuff as i64);
    let scholar_uptime = match player.get_damage_mod_uptime(DamageModID::Scholar as i32) {
        None => 0.0,
        Some(uptime) => 100.0 * uptime.get_uptime_hits(),
    };
    let mut uptimes = HashMap::new();
    uptimes.insert(GW2SkillID::QuicknessBuff as i32, quickness_uptime);
    uptimes.insert(GW2SkillID::AlacrityBuff as i32, alac_uptime);
    uptimes.insert(GW2SkillID::ProtectionBuff as i32, prot_uptime);
    uptimes.insert(GW2SkillID::MightBuff as i32, might_uptime);
    uptimes.insert(DamageModID::Scholar as i32, scholar_uptime);
    let mut group_gen = HashMap::new();
    group_gen.insert(GW2SkillID::QuicknessBuff as i32, quickness_group);
    group_gen.insert(GW2SkillID::AlacrityBuff as i32, alac_group);
    let mut off_group_gen = HashMap::new();
    off_group_gen.insert(GW2SkillID::QuicknessBuff as i32, quickness_off_group);
    off_group_gen.insert(GW2SkillID::AlacrityBuff as i32, alac_off_group);
    let mut roles = None;
    for tank_player in tank_players {
        if player.name() == tank_player.name() {
            roles = Some(vec!["tank".to_string()])
        }
    }
    for special in special_players {
        if player.name() == special.name() {
            roles = Some(vec!["special".to_string()])
        }
    }
    ShortLogPlayer {
        target_cc: player.get_all_targets_cc(),
        target_dps: player.get_all_targets_dps(),
        //build: builds::get_player_build(player),
        build_hash: build_guesser.guess_build(player).hash,
        buff_uptime: uptimes,
        group_buff_generation: group_gen,
        off_group_buff_generation: off_group_gen,
        sub_group: player.sub_group(),
        name: player.name().to_owned(),
        roles,
        active_time: player
            .active_time(PhaseIndex::full_fight())
            .unwrap_or_else(|| Duration::milliseconds(1))
            .num_milliseconds() as u64,
    }
}

// TODO this clones the player
fn get_top_players_by_mechanic(
    ei_data: &EliteInsightData,
    mechanic_name: &str,
    count: usize,
    before_ms: Option<Duration>,
    min_count: u32,
) -> Vec<EIPlayer> {
    let mechanic_counts =
        ei_data.get_mechanic_counts(mechanic_name, before_ms.map(|dur| dur.num_milliseconds()));
    let mut players_filtered = ei_data
        .players
        .clone()
        .iter()
        .filter(|player| {
            mechanic_counts.contains_key(player.name())
                && mechanic_counts.get(player.name()).unwrap() >= &min_count
        })
        .cloned()
        .collect::<Vec<EIPlayer>>();
    players_filtered.sort_by(|p1, p2| {
        mechanic_counts
            .get(p2.name())
            .unwrap()
            .cmp(mechanic_counts.get(p1.name()).unwrap())
    });

    let last = min(count, players_filtered.len());
    players_filtered[0..last].to_vec()
}

fn get_toughness_tanks(ei_data: &EliteInsightData, count: usize) -> Vec<EIPlayer> {
    let mut toughness_players = ei_data
        .players
        .iter()
        .filter(|player| player.toughness_rank() > 0 && player.friendly_npc != Some(true))
        .cloned()
        .collect::<Vec<EIPlayer>>();
    toughness_players.sort_by_key(|player| std::cmp::Reverse(player.toughness_rank()));
    let last = min(count, toughness_players.len());
    toughness_players[0..last].to_vec()
}
