use anyhow::anyhow;
use chrono::IsoWeek;
use gloo_console::{error, log, warn};
use indexed_db_futures::idb_object_store::{IdbObjectStore, IdbObjectStoreParameters};
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::future::IntoFuture;
use std::hash::Hash;

use gloo_utils::format::JsValueSerdeExt;

use indexed_db_futures::request::{IdbOpenDbRequestLike, OpenDbRequest};

use indexed_db_futures::{IdbDatabase, IdbKeyPath, IdbQuerySource, IdbVersionChangeEvent};

use wasm_bindgen::JsValue;
use web_sys::IdbTransactionMode::{Readonly, Readwrite};
use web_sys::{DomException, IdbIndexParameters};

use crate::builds::BuildGuesser;
use crate::short_log_data::OldShortLogDataV16;

const LOGS_STORE_NAME: &str = "logs";
const BUILDS_STORE_NAME: &str = "builds";
const DB_NAME: &str = "LogsData";
// v14: add week index to log entries
// v15: same as 14
// v16: same as 14
// v17: add active time to player
const DB_VERSION: u32 = 17;

pub struct IdbHandle {
    idbdb: IdbDatabase,
}

fn add_all_builds(store: IdbObjectStore<'_>) {
    let builds = BuildGuesser::new().get_builds();
    for build in builds.iter() {
        let js_value = <JsValue as JsValueSerdeExt>::from_serde(&build)
            .expect("Failed to convert build to JsValue");
        let key = build.hash;
        log!(format!("adding {key}:"));
        store
            .put_val_owned(js_value)
            .expect("Failed to add build to db");
    }
}

impl<'db> IdbHandle {
    pub async fn open() -> IdbHandle {
        let db = IdbDatabase::open(DB_NAME)
            .expect("failed to open db open request")
            .into_future()
            .await
            .expect("Failed to open db");
        let old_version = db.version();
        if old_version <= 16.0
            && db
                .object_store_names()
                .any(|store_name| store_name == LOGS_STORE_NAME)
        {
            log!("Migrating log storage, new data required deleting old data");
        }
        drop(db);
        let mut db_req: OpenDbRequest =
            IdbDatabase::open_u32(DB_NAME, DB_VERSION).expect("failed to open db open request");
        db_req.set_on_upgrade_needed(Some(
            move |event: &IdbVersionChangeEvent| -> Result<(), JsValue> {
                // Check if the object store exists; create it if it doesn't
                log!(
                    "upgrading indexed db from version",
                    event.old_version(),
                    "to",
                    event.new_version()
                );
                if !event
                    .db()
                    .object_store_names()
                    .any(|n| n == BUILDS_STORE_NAME)
                {
                    // add builds table

                    log!("creating builds db");
                    let mut store_params = IdbObjectStoreParameters::new();
                    store_params.key_path(Some(&IdbKeyPath::str("hash")));
                    let store = event
                        .db()
                        .create_object_store_with_params(BUILDS_STORE_NAME, &store_params)
                        .expect("Faild to create build store");
                    //let mut index_params = IdbIndexParameters::new();
                    //index_params.unique(true);
                    //index_params.multi_entry(false);
                    //index_params.locale(None);
                    //store.create_index_with_params("hash", &IdbKeyPath::str("hash"), &index_params)
                    //    .unwrap();
                    // insert all builds
                    add_all_builds(store);
                }
                if event
                    .db()
                    .object_store_names()
                    .any(|store_name| store_name == LOGS_STORE_NAME)
                {
                    if event.old_version() <= 15.0 {
                        log!("Adding week index to log storage");
                        let t = event.transaction();
                        let s = t.object_store(LOGS_STORE_NAME).unwrap();
                        let params = IdbIndexParameters::new();
                        params.set_unique(false);
                        params.set_multi_entry(false);
                        params.set_locale(None);
                        s.create_index_with_params("week", &IdbKeyPath::str("week"), &params)
                            .unwrap();
                    }
                }
                if event
                    .db()
                    .object_store_names()
                    .find(|n| n == LOGS_STORE_NAME)
                    .is_none()
                {
                    let store = event.db().create_object_store(LOGS_STORE_NAME).unwrap();
                    let params = IdbIndexParameters::new();
                    params.set_unique(false);
                    params.set_multi_entry(false);
                    params.set_locale(None);
                    store
                        .create_index_with_params("boss_id", &IdbKeyPath::str("boss_id"), &params)
                        .unwrap();
                    store
                        .create_index_with_params("week", &IdbKeyPath::str("week"), &params)
                        .unwrap();
                }
                Ok(())
            },
        ));
        let handle = IdbHandle {
            idbdb: db_req.into_future().await.expect("Failed to open db"),
        };
        if handle.idbdb.version() != old_version {
            log!("Upgraded database, version is now", handle.idbdb.version());
        }
        handle
    }

    async fn migrate_log_data<O, N>(db: &IdbDatabase) -> Result<(), DomException>
    where
        O: Into<N> + for<'a> serde::Deserialize<'a>,
        N: serde::Serialize,
    {
        let data: HashMap<String, O> = Self::get_all_logs_data_from_handle(db).await?;
        for (url, old) in data {
            let new: N = old.into();
            Self::add_log_data_from_handle(db, &url, &new).unwrap();
            log!("Migrated", url.clone());
        }
        Ok(())
    }

    async fn clear_log_data(db: &IdbDatabase) -> Result<(), anyhow::Error> {
        let data: HashMap<String, OldShortLogDataV16> = Self::get_all_logs_data_from_handle(db)
            .await
            .map_err(|err| anyhow!(err.as_string().unwrap()))?;
        for (url, _) in data {
            log!("migrating", url.clone());
            let url = format!("https://dps.report/{url}");
            Self::remove_log_data(db, &url).await;
        }
        Ok(())
    }

    pub fn add_log_data<T>(&self, url: &str, value: &T) -> Result<(), DomException>
    where
        T: ?Sized + serde::Serialize,
    {
        Self::add_log_data_from_handle(&self.idbdb, url, value)
    }

    pub fn add_log_data_from_handle<T>(
        db: &IdbDatabase,
        url: &str,
        value: &T,
    ) -> Result<(), DomException>
    where
        T: ?Sized + serde::Serialize,
    {
        let transaction = db
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readwrite)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME)?;
        let js_value = <JsValue as JsValueSerdeExt>::from_serde(&value).unwrap();
        let key = url.split("dps.report/").last().unwrap();
        store.put_key_val_owned(key, &js_value)?;
        Ok(())
    }

    pub async fn has_log_data(&self, url: &str) -> bool {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readonly)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME).unwrap();
        let key = url.split("dps.report/").last().unwrap();
        let result = store
            .open_cursor_with_range(&JsValue::from_str(key))
            .unwrap()
            .await;
        match result {
            Ok(option) => option.is_some(),
            Err(_) => false,
        }
    }

    pub async fn get_build<T>(&self, build_hash: u64) -> Option<T>
    where
        T: for<'a> serde::Deserialize<'a>,
    {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(BUILDS_STORE_NAME, Readonly)
            .unwrap();
        let store: indexed_db_futures::prelude::IdbObjectStore<'_> = transaction
            .object_store(BUILDS_STORE_NAME)
            .expect("Failed to open build store");
        store
            .get_owned(build_hash as f64)
            .expect("Failed to find key")
            .await
            .unwrap()
            .map(|value| JsValueSerdeExt::into_serde(&value).unwrap())
    }

    pub async fn get_log_data<T>(&self, url: &str) -> Option<T>
    where
        T: for<'a> serde::Deserialize<'a>,
    {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readonly)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME).unwrap();
        let key: &str = url.split("dps.report/").last().unwrap();
        store
            .get_owned(key)
            .unwrap()
            .await
            .unwrap()
            .map(|value| JsValueSerdeExt::into_serde(&value).unwrap())
    }

    pub async fn get_logs_for_boss<T, U>(
        &self,
        boss_id: &str,
        transformer: fn(String, T) -> U,
    ) -> Option<Vec<U>>
    where
        T: for<'a> serde::Deserialize<'a>,
    {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readonly)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME).unwrap();
        let range = JsValue::from_str(boss_id);
        let index = store.index("boss_id").unwrap();
        let cursor = index.open_cursor_with_range(&range).unwrap();
        let db_result = cursor.await;
        match db_result {
            Ok(Some(cursor_result)) => {
                let mut result = Vec::<U>::new();
                loop {
                    let url = cursor_result.primary_key().unwrap();
                    match JsValueSerdeExt::into_serde(&cursor_result.value()) {
                        Ok(value) => {
                            result.push(transformer(url.as_string().unwrap(), value));
                        }
                        Err(err) => {
                            error!("Failed to load log", &url, err.to_string());
                            //store.delete(&url).unwrap();
                        }
                    };

                    if !cursor_result.continue_cursor().unwrap().await.unwrap() {
                        break;
                    }
                }
                Some(result)
            }
            Ok(None) => {
                log!("No value", range);
                None
            }
            Err(err) => {
                log!(err);
                None
            }
        }
    }

    // todo return u64 instead
    pub async fn get_all_builds<T>(&self) -> Result<Vec<T>, DomException>
    where
        T: for<'a> serde::Deserialize<'a>,
    {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(BUILDS_STORE_NAME, Readonly)
            .unwrap();
        let store = transaction.object_store(BUILDS_STORE_NAME)?;
        let cursor_result = store.open_cursor()?.await?.unwrap();
        let mut all_data = Vec::new();
        loop {
            let value = JsValueSerdeExt::into_serde(&cursor_result.value())
                .expect("Failed to comvert to serde");
            all_data.push(value);
            if !cursor_result.continue_cursor().unwrap().await.unwrap() {
                break;
            }
        }
        Ok(all_data)
    }

    pub async fn get_all_logs_data<T>(&self) -> Result<HashMap<String, T>, DomException>
    where
        T: for<'a> serde::Deserialize<'a>,
    {
        IdbHandle::get_all_logs_data_from_handle(&self.idbdb).await
    }

    async fn get_all_logs_data_from_handle<T>(
        db: &IdbDatabase,
    ) -> Result<HashMap<String, T>, DomException>
    where
        T: for<'a> serde::Deserialize<'a>,
    {
        let transaction = db
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readonly)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME)?;
        let cursor_result = store.open_cursor()?.await?.unwrap();
        let mut all_data = HashMap::new();
        loop {
            let url = cursor_result.primary_key().unwrap().as_string().unwrap();
            match JsValueSerdeExt::into_serde(&cursor_result.value()) {
                Ok(value) => {
                    all_data.insert(url, value);
                }
                Err(err) => {
                    warn!("log is invalid:", &url, err.to_string());
                    //store.delete(&JsValue::from_str(&url)).unwrap();
                }
            }

            if !cursor_result.continue_cursor().unwrap().await.unwrap() {
                break;
            }
        }
        Ok(all_data)
    }

    pub async fn get_logs_filtered_grouped<F, U, G>(
        &self,
        filter: fn(&F) -> bool,
        transformer: fn(String, F) -> U,
        group_fn: fn(&F) -> G,
    ) -> Option<HashMap<G, Vec<U>>>
    where
        F: for<'a> serde::Deserialize<'a>,
        G: Eq + Hash,
    {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readonly)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME).unwrap();
        let cursor = store.open_cursor().unwrap();
        let db_result = cursor.await;
        match db_result {
            Ok(value) => match value {
                Some(cursor_result) => {
                    let mut result = HashMap::<G, Vec<U>>::new();
                    loop {
                        let url = cursor_result.primary_key().unwrap();
                        let value = JsValueSerdeExt::into_serde(&cursor_result.value()).unwrap();
                        if filter(&value) {
                            let key = group_fn(&value);
                            let vt = transformer(url.as_string().unwrap(), value);
                            match result.entry(key) {
                                Entry::Occupied(mut o) => o.get_mut().push(vt),
                                Entry::Vacant(v) => {
                                    v.insert(vec![vt]);
                                }
                            };
                        }
                        if !cursor_result.continue_cursor().unwrap().await.unwrap() {
                            break;
                        }
                    }
                    log!("[DB FL RE]");
                    Some(result)
                }
                None => {
                    log!("[DB FL] No value");
                    None
                }
            },
            Err(value) => {
                log!(value);
                None
            }
        }
    }

    pub async fn get_logs_filtered_grouped_by_week<F, U>(
        &self,
        week: IsoWeek,
        filter: fn(&F) -> bool,
        transformer: fn(String, F) -> U,
    ) -> Option<Vec<U>>
    where
        F: for<'a> serde::Deserialize<'a>,
    {
        let transaction = self
            .idbdb
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readonly)
            .unwrap();

        let store = transaction.object_store(LOGS_STORE_NAME).unwrap();
        let range = JsValue::from_f64((week.week() + week.year() as u32 * 100) as f64);
        let index = store.index("week").unwrap();
        let cursor = index.open_cursor_with_range(&range).unwrap();
        let db_result = cursor.await;
        match db_result {
            Ok(value) => match value {
                Some(cursor_result) => {
                    let mut result = vec![];
                    loop {
                        let url = cursor_result.primary_key().unwrap();
                        match JsValueSerdeExt::into_serde(&cursor_result.value()) {
                            Ok(value) => {
                                if filter(&value) {
                                    let vt = transformer(url.as_string().unwrap(), value);
                                    result.push(vt);
                                }
                            }
                            Err(err) => {
                                warn!(
                                    "Failed to load log:",
                                    &url,
                                    err.to_string(),
                                    &cursor_result.value()
                                );
                                //store.delete(&url).unwrap().await.unwrap();
                            }
                        }

                        if !cursor_result.continue_cursor().unwrap().await.unwrap() {
                            break;
                        }
                    }
                    Some(result)
                }
                None => {
                    // no logs in that week
                    Some(vec![])
                }
            },
            Err(value) => {
                log!(value);
                None
            }
        }
    }
    pub async fn remove_log_data(idb: &IdbDatabase, url: &str) {
        let transaction = idb
            .transaction_on_one_with_mode(LOGS_STORE_NAME, Readwrite)
            .unwrap();
        let store = transaction.object_store(LOGS_STORE_NAME).unwrap();
        store
            .delete(&JsValue::from_str(url.split('/').last().unwrap()))
            .unwrap()
            .await
            .unwrap();
        log!("deleted ", url);
    }
}
